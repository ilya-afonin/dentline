<?php
$arUrlRewrite=array (
  0 => 
  array (
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 100,
  ),
  1 => 
  array (
    'CONDITION' => '#^/persons/([0-9]+)/(\\?.*)?#',
    'RULE' => 'ELEMENT_ID=$1',
    'PATH' => '/persons/detail.php',
    'SORT' => 200,
  ),
  2 => 
  array (
    'CONDITION' => '#^/offers/([0-9]+)/(\\?.*)?#',
    'RULE' => 'ELEMENT_ID=$1',
    'PATH' => '/offers/detail.php',
    'SORT' => 300,
  ),
  3 => 
  array (
    'CONDITION' => '#^/special/([0-9]+)/(\\?.*)?#',
    'RULE' => 'ELEMENT_ID=$1',
    'PATH' => '/special/detail.php',
    'SORT' => 400,
  ),
);
