<?
	use Bitrix\Main\Loader;
	use Bitrix\Main\Page\Asset;
	use Bitrix\Main\Localization\Loc;
	
  if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
  if (!Loader::includeModule("iblock")) return;

  Loc::loadMessages(__FILE__);
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/assets/css/lib/scrollbar.css");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/assets/js/custom.map.js", true);
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/assets/js/infobox.js", true);
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/assets/js/lib/scrollbar.js", true);
  Asset::getInstance()->addJs("https://maps.googleapis.com/maps/api/js?key=фывфыв=MapConstructor.load", true);

?>
<div class="map-wrapper">
  <div id="i-map" class="map i-map">
    <div id="userUI"></div>
  </div>
  <div class="map-info__opener">
    <div class="map-info__opener-text"><?= Loc::getMessage('I_MAP_TITLE'); ?></div>
  </div>

  <div class="map-info map-info--interactive">
    <div class="map-info__title"><?= Loc::getMessage('I_MAP_TITLE'); ?></div>
    <?
      $selectedCategory = !empty($_REQUEST['category'])? explode(':',$_REQUEST['category']):array();
      $selectedCategory = array_unique($selectedCategory);

      foreach($selectedCategory as $key=>$arCategory){
        $subArray[] = array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 7, "PROPERTY_SERVICES" => $arCategory, "PROPERTY_CITY"=>"%".$_REQUEST["ID_TO_SHOW"]."%")));
      }

      $rs = CIBlockElement::GetList(
          array(),
          array(
              "IBLOCK_ID" => 7,
              $subArray
          ),
          false,
          false,
          array("ID")
      );
      $elements_id = array();
      while ($ar = $rs->GetNext()) {
        $elements_id[] = $ar['ID'];
      }



	/*spoc добавил*/

		if(empty($_REQUEST["ID_TO_SHOW"]) && empty($_REQUEST["category"]) && isset($_REQUEST["category"]) && isset($_REQUEST["ID_TO_SHOW"]))
		{
			$APPLICATION->RestartBuffer();
			$APPLICATION->ShowHead();
		}

		if($_REQUEST["ID_TO_SHOW"] || $_REQUEST["category"])
		{
			$APPLICATION->RestartBuffer();
			if(empty($_REQUEST["ID_TO_SHOW"]))
				$APPLICATION->ShowHead();
		}
	/*---*/
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/jquery.jscrollpane.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/jquery.mousewheel.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/jScrollPane.js");



 	$GLOBALS['categoriesMapFilter'] = array("ID" => $elements_id);

      $APPLICATION->IncludeComponent("bitrix:news.list", "i-map", Array(
      "ACTIVE_DATE_FORMAT" => "d.m.Y",
      "CACHE_GROUPS" => "Y",
      "CACHE_TIME" => "300",
      "CACHE_TYPE" => "Y",
	//"FIELD_CODE" => array_merge((Array)$arParams['FIELD_CODE'], (Array)$arParams['PROPERTY_CODE']),
      "FIELD_CODE" => $arParams["FIELD_CODE"],
      "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
      "IBLOCK_ID" => $arParams['IBLOCK_ID'],
      "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
      "NEWS_COUNT" => "300",
      "SORT_BY1" => "ACTIVE_FROM",
      "SORT_BY2" => "SORT",
      "AJAX_MODE" => "Y",
      "AJAX_OPTION_HISTORY" => "N",
      "AJAX_OPTION_JUMP" => "N",
      "AJAX_OPTION_STYLE" => "Y",
      "SORT_ORDER1" => "DESC",
      "SORT_ORDER2" => "ASC",
      "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
      "FILTER_NAME"=>"categoriesMapFilter",
      "CATEGORY"=>$selectedCategory,

    ), $component);
?>
<?
/*spoc добавил*/
if((empty($_REQUEST["ID_TO_SHOW"])&& isset($_REQUEST["ID_TO_SHOW"])) || (empty($_REQUEST["category"]) && isset($_REQUEST["category"])))
	die();
if($_REQUEST["ID_TO_SHOW"])
	die();
/*-----*/
?>
  </div>
  <a href="#" class="map-move"></a>
</div>

<?
  function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
  }

  $zoom = 3;

  if(isMobile()) {
    $zoom = 2;
  }

  $arResult['JS_OBJECT'] = array(
      'id' => 'i-map',
      'options' => array(
          'zoom' => $zoom,
          'scrollwheel' => true,
          'gestureHandling' =>  'greedy',
          'disableDefaultUI' => true,
          'center' => array(
              47.993937,
              100.77914299999998
          ),
          'mapTypeControl' => true,
          'mapTypeId' => 'roadmap'
      ),
      'shops' => array()
  );
?>
<script>
	new MapConstructor(<?=CUtil::PhpToJSObject($arResult['JS_OBJECT'])?>).mapInitialize();


	$(document).on("click", ".opt_txt", function (){
		$.ajax({
			url: '<?$APPLICATION->GetCurPage();?>',
			type: "POST",
			data: {ID_TO_SHOW:$(this).text(),category: 664},
			success: function(data)
				{
					var p ='<div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside"><div class="map-info__title">Партнеры программы в регионах: </div>'+data+'</div>';
					$(".map-info.map-info--interactive").html(p);
					$(function()
					{
						$('#mCSB_1').jScrollPane();
					});
				},
			error: function(msg){ }
		});
	});

	$(document).on("keyup", "#searchInput", function (){
		$(".test2").addClass("oppener");
		
		var _this = $(this);
		$( ".opt_txt" ).each(function( ) {
			if(($(this).text().toLowerCase().indexOf(_this.val().toLowerCase())+1)>0) {
				$(this).css("display","block");
			}else{
				$(this).css("display","none");
			}
		});
		$( ".map-info__city" ).each(function( ) {
			if(($(this).find(".map-info__city-title").text().toLowerCase().indexOf(_this.val().toLowerCase())+1)>0) {
				$(this).css("display","block");
			}else{
				$(this).css("display","none");
			}
		});
	});

	$(document).on("click", ".map-show-all", function (event){
	var reset = false;
		if(!reset)
		{
			reset = true;
			document.location.reload(false);
		}
	});

	$(document).on("click", ".clos-x", function (){
		$(this).addClass("oppen");
		$("div.test2").addClass("oppener");
		$("div.test2 span").css({"padding-top":"5px","padding-bottom":"5px"});
		$("div.test2 span:first-child").css({"display":"none"});
		$("div.test2.oppener").css({"top":"25px"});
		$("body input.spisok-gorodov-input").css({"width":"90%"});
	});

	$(document).on("click", ".oppen", function (){
		$(".test2").removeClass("oppener");
		$(this).removeClass("oppen");
		$("div.test2").css({"top":"0px"});
		$("div.test2 span:first-child").css({"display":"block"});
		$("div.test2 span:first-child").css({"display":"block"});
	});

	$(".infobox__icon").each(function() {
	  if(($(this).attr("title")) == "Список городов")
		{
			$(this).css({"display":"none"});
		}
	});

	$(document).ready(function() {
		$("#i-map > div > div > div:nth-child(1) > div:nth-child(3) > div").removeClass("gmnoprint");
	});

	$(document).on("click", ".gmnoprint", function (){
			$.ajax({
					url: "/ajaxCity.php",
					type: "POST",
					data: {	TITLE_OF_CITY:$(this).attr('title') },
					success: function(data)
					{
						var r = JSON.parse(data);
						$(".map-info__cities .map-info__city").each(function() {
							if(r.indexOf($(this).find(".map-info__city-title").text()) == -1)
								$(this).css("display","none");
						});
					}
			});
		});
</script>



