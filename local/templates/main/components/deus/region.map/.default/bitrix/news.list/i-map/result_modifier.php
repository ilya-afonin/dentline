<?
  if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
  }
$arResult['POINTS'] = array();

$categoriesId = array();
foreach($arResult["ITEMS"] as &$arItem){

  $arItem['PROPERTY_PHONE_VALUE'] = (array)$arItem['PROPERTY_PHONE_VALUE'];

  $shop = array(
    'ID' => $arItem['ID'],
    'LINK_ID' => $this->GetEditAreaId($arItem['ID']),
    'ADDRESS' => $arItem['DISPLAY_PROPERTIES']['ADDRESS']['VALUE'],
    'NAME' => $arItem['NAME'],
    'PHONE' =>  $arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'],
    'EMAIL' => $arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'],
    'LOGO' => SITE_TEMPLATE_PATH.'/tpl/assets/images/static/map-dot.png',
  );
  $arCoordinate = explode(',', $arItem['DISPLAY_PROPERTIES']['COORDINATE']['VALUE']);
  if(count($arCoordinate) == 2)
  $shop = array_merge($shop, array_combine(array(
    'LAT',
    'LNG'
  ), $arCoordinate));
  $arResult['POINTS'][] = $shop;
  $categoriesId+=  (Array)$arItem['DISPLAY_PROPERTIES']['SERVICES']['VALUE'];

  $arService = Bitrix\Iblock\ElementTable::getList(array(
      'filter' => array('IBLOCK.CODE' => 'center_sevices', 'ID' => $arItem['PROPERTIES']['SERVICES']['VALUE']),
      'select' => array('PREVIEW_PICTURE', 'ID', 'NAME'),
  ));

  while ($arS = $arService->fetch()) {
    $File = CFile::ResizeImageGet($arS['PREVIEW_PICTURE'], array(
        'width' => 30,
        'height' => 30
    ), BX_RESIZE_IMAGE_PROPORTIONAL, true);

    if (!empty($File['src'])) {
      foreach ($File as $key => $value) {
        $File[strtoupper($key)] = $value;
        unset($File[$key]);
      }
      $arS['ICON'] = $File;
    }
    $arItem['SERVICES'][$arS['ID']] = $arS;
  }

}
  $categoryIterator = Bitrix\Iblock\ElementTable::getList(array(
    'filter'=>array('IBLOCK.CODE'=>'center_sevices'),
    'select'=>array('PREVIEW_PICTURE','ID', 'NAME'),
  ));
  $categories = array();
  while($ar = $categoryIterator->fetch()){
    $file = CFile::ResizeImageGet($ar['PREVIEW_PICTURE'], array(
      'width' => 30,
      'height' => 30
    ), BX_RESIZE_IMAGE_PROPORTIONAL, true);

    if(!empty($file['src'])){
      foreach($file as $key=>$value){
        $file[strtoupper($key)] = $value;
        unset($file[$key]);
      }
      $ar['ICON'] = $file;
    }
    $categories[$ar['ID']] = $ar;
  }
  $arResult['CATEGORIES'] = $categories;
  
?>
