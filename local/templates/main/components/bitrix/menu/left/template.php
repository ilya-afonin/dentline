<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
    <nav class="sidenav is-sticky">
        <ul class="sidenav__list">
            <?php foreach($arResult as $k => $arItem){ ?>
                <li class="sidenav__item<?=($k==0)?' sidenav__item--active':''?>">
                    <a class="sidenav__link" href="#" data-anchor="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                </li>
            <?php } ?>
        </ul>
    </nav>
<?php } ?>