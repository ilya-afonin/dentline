<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
    <nav class="h__nav-wrapper">
        <ul class="h__nav">
    <?php foreach($arResult as $arItem){ ?>
        <li class="h__nav-item"><a class="h__nav-link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
    <?php } ?>
        </ul>
  </nav>
<?php } ?>