<?php
foreach ($arResult['ITEMS'] as $k => $arItem){
    $img = CFile::ResizeImageGet(
        $arItem['PREVIEW_PICTURE']['ID'],
        array("width" => 65, "height" => 65),
        BX_RESIZE_IMAGE_EXACT,
        true,
        false
    );
    $arResult['ITEMS'][$k]['RESIZED_PICTURE']['SRC'] = $img['src'];

}
