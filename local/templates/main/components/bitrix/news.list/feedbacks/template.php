<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<? if (count($arResult['ITEMS']) > 0): ?>
<div class="f-slider-wrapper">
    <div class="f-slider">
        <div class="f-slider__scene owl-carousel owl-height">
        <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="f-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="f-image">
                    <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>
                        <img class="f-image-img" src="<?= $arItem['RESIZED_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                    <? endif; ?>
                </div>
                <div class="f-content">
                  <div class="f-text"><?=$arItem['PREVIEW_TEXT']?></div>
                  <?if(!empty($arItem['PROPERTIES']['AUTHOR']['VALUE'])):?>
                    <div class="f-author">— <?=$arItem['PROPERTIES']['AUTHOR']['VALUE']?></div>
                  <?endif;?>
                </div>
            </div>
        <? endforeach; ?>
        </div>
    </div>
</div>
<?endif;?>