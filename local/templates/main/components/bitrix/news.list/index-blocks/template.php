<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<? if (count($arResult['ITEMS']) >= 3): ?>
    <div class="col-12 col-lg-6">
        <? foreach ($arResult["ITEMS"] as $i => $arItem): ?>
            <?
            if($i % 3 == 0){
                $class = 'index__block-item--small1';
            } elseif ($i % 3 == 1){
                $class = 'index__block-item--small2';
             } elseif ($i % 3 == 2){
                $class = 'index__block-item--big';
            }
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
        <?if($i % 3 == 2):?>
            </div>
            <div class="col-12 col-lg-6 wrapper-big">
        <?endif;?>
            <div class="index__block-item <?=$class?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <a class="index__block-link-wrapper" href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>"></a>
                <div class="index__block-text">
                    <h2 class="index__block-name"><?=$arItem['NAME']?></h2>
                    <a class="index__block-link" href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
                        Подробнее<svg width="7" height="11" viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L5.5 5.5L1 10" stroke="black" stroke-width="1.5"/></svg>
                    </a>
                </div>
                <div class="index__block-image">
                    <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>
                        <img class="index__block-image-img" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                    <? endif; ?>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>
