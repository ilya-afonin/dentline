<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? if (count($arResult['ITEMS']) > 0): ?>
    <div class="prices">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="price__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="price">
                    <div class="price__top"><?= $arItem['NAME'] ?></div>
                    <div class="price__list">
                        <div class="price__list-item">
                            <div class="price__name"><?= $arItem["PREVIEW_TEXT"] ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach ?>
    </div>
<? endif; ?>