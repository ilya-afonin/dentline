<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? if (count($arResult['ITEMS']) > 0): ?>
<div class="t-slider-wrapper">
  <div class="t-slider">
    <div class="t-slider__scene owl-carousel">
        <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
          <div class="t-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <?if($APPLICATION->GetCurDir() == '/stomatology/' && $arItem['PROPERTIES']['NO_LINK']['VALUE'] == 0):?>
              <a class="t-link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                <div class="t-image">
                  <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>
                    <img class="t-image-img" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                  <? endif; ?>
                </div>
              </a>
            <?else:?>
              <div class="t-image">
                <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>
                  <img class="t-image-img" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                <? endif; ?>
              </div>
            <?endif;?>
            <div class="t-text">
              <?if($APPLICATION->GetCurDir() == '/stomatology/' && $arItem['PROPERTIES']['NO_LINK']['VALUE'] == 0):?>
              <a class="t-link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                <div class="t-name"><?=$arItem['NAME']?></div>
              </a>
              <?else:?>
                <div class="t-name"><?=$arItem['NAME']?></div>
              <?endif;?>
              <div class="t-post"><?=$arItem['PROPERTIES']['POSITION']['VALUE']?></div>
            </div>
          </div>
        <? endforeach; ?>
    </div>
    <div class="p-slider__controls">
        <div class="p-slider__controls-inner"></div>
    </div>
</div>
<?endif;?>
