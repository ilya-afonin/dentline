<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? if (count($arResult['ITEMS']) > 0): ?>
<div class="w-slider-wrapper">
    <div class="w-slider">
        <div class="w-slider__scene owl-carousel">
        <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="w-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <a class="w-link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <div class="w-image">
                        <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?> 
                            <img class="w-image-img" src="<?= $arItem['RESIZED_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>"
                                 data-fancybox="group<?=$arItem['ID']?>" data-src="<?= $arItem['PREVIEW_PICTURE']['SRC']?>">
                        <? endif; ?>

                        <?if (!empty($arItem["PROPERTIES"]["MORE_PHOTOS"]["VALUE"])) {?>
                            <div class="w-image-fancy">
                                <?foreach ($arItem["PROPERTIES"]["MORE_PHOTOS"]["VALUE"] as $photo) {?>
                                    <a data-fancybox="group<?=$arItem['ID']?>" href="<?= $photo ?>">
                                        <img src="<?= $arItem['RESIZED_PICTURE']['SRC'] ?>" alt="">
                                    </a>
                                <?}?>
                            </div>
                        <? } ?>

                    </div>
                </a>
                <div class="w-text">
                    <a class="w-link" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <div class="w-name"><?= $arItem['NAME'] ?></div>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
        </div>
    </div>
</div>
<?endif;?>