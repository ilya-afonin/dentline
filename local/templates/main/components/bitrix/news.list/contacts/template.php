<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
$arItem = $arResult['ITEMS'][0];

$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<div class="contacts__top row" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
    <div class="col-12 col-lg-6">
        <div class="contacts__title"><?=Loc::getMessage('REQUISITES')?></div>
        <p class="contacts__text"><?= $arItem["PREVIEW_TEXT"] ?></p>
    </div>
    <div class="col-12 col-lg-5 offset-lg-1">
        <div class="contacts__title"><?=Loc::getMessage('CONTACTS')?></div>
        <p class="contacts__text">
            <span><?=Loc::getMessage('PHONE')?></span> <?= \COption::GetOptionString("askaron.settings", "UF_PHONE") ?><br>
            <span><?=Loc::getMessage('MAIL')?></span> <?= \COption::GetOptionString("askaron.settings", "UF_MAIL") ?><br>
            <span><?=Loc::getMessage('GRAFIK')?></span> <?= \COption::GetOptionString("askaron.settings", "UF_GRAFIK") ?></p>
    </div>
</div>

