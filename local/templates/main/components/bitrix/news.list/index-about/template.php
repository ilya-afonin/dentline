<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<? if (count($arResult['ITEMS']) > 0): ?>
            <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="index__about-block row" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <div class="index__about-image col-12 col-lg-5 wow fadeIn">
                        <?if(!empty($arItem['PREVIEW_PICTURE']['SRC'])):?>
                            <img class="index__about-image-img" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="О компании"></div>
                        <?endif?>
                    <div class="index__about-content col-12 col-lg-6 offset-lg-1 wow fadeIn">
                        <?= $arItem['PREVIEW_TEXT'] ?>
                    </div>
                </div>
            <? endforeach; ?>
<? endif; ?>