<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? if (count($arResult['ITEMS']) > 0): ?>
    <div class="prices">
        <? foreach ($arResult["ITEMS"] as $arSection): ?>
            <div class="price__item">
                <div class="price">
                    <div class="price__top"><?= $arSection['NAME'] ?></div>
                    <div class="price__list">
                        <? if ($arSection['SHOW_DISCOUNT']): ?>
                            <div class="price__head">
                                <div class="price__head-name"></div>
                                <div class="price__head-price">цена</div>
                                <div class="price__head-price price__head-price--discount">цена<br/>по акции</div>
                            </div>
                        <?endif?>
                        <? foreach ($arSection["ELEMENTS"] as $key => $arItem): ?>
                            <div class="price__list-item">
                                <div class="price__name"><?= $arItem["NAME"] ?></div>
                                <? if (!empty($arItem['PROPERTIES']['PRICE']['VALUE'])): ?>
                                    <div class="price__price price__price--real"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?> ₽</div>
                                <? endif; ?>
                                <? if ($arSection['SHOW_DISCOUNT']): ?>
                                    <div class="price__price price__price--discount">
                                        <? if (!empty($arItem['PROPERTIES']['PRICE_DISCOUNT']['VALUE'])): ?>
                                            <?= $arItem['PROPERTIES']['PRICE_DISCOUNT']['VALUE'] ?> ₽
                                        <?endif;?>
                                    </div>
                                <? endif; ?>
                            </div>
                        <? endforeach ?>
                    </div>
                </div>
            </div>
        <? endforeach ?>
    </div>
<? endif; ?>