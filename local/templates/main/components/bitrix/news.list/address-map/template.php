<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<? if (count($arResult['ITEMS']) > 0): ?>

  <?$arItem = $arResult['ITEMS'][0];?>

  <div class="s-wrapper">
    <div class="s-title"><?=$arItem['PROPERTIES']['ADDRESS']['VALUE']?></div>
  </div>
  <div class="contacts__map wow fadeIn">
    <div class="map map_one" id="mapOne" data-points="<?=$arItem['PROPERTIES']['POINTS']['VALUE']?>"></div>
  </div>

<? endif; ?>
