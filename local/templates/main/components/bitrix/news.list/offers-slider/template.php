<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<? if (count($arResult['ITEMS']) > 0): ?>
    <div class="p-slider">
        <div class="p-slider__scene owl-carousel">
            <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="p-slider__slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                    <div class="p-slider__image">
                        <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])): ?>
                            <img class="p-slider__image-img" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem['NAME'] ?>">
                        <? endif; ?>
                    </div>
                    <div class="p-slider__right">
                        <div class="p-slider__text-wrapper">
                            <div class="p-slider__text">
                                <?= $arItem['PREVIEW_TEXT'] ?>
                            </div>
                            <a class="btn" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" role="button"><?= $arItem['PROPERTIES']['BUTTON_TXT']['VALUE'] ?></a>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="p-slider__controls">
            <div class="p-slider__controls-inner"></div>
        </div>
    </div>
<? endif; ?>