<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
Loc::loadMessages(__FILE__);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (!empty($arResult["ITEMS"])):
?>

<div class="contacts__filter row">
    <div class="contacts__groups col-12 col-lg-4">
        <div class="c-filter__title">Выбрать город</div>
        <select class="select-lab select-cities" id="cities-list">
            <?foreach($arResult['SECTIONS'] as $arCity):?>
                <option data-id="<?=$arCity['ID']?>" value="<?=$arCity['NAME']?>"><?=$arCity['NAME']?></option>
            <?endforeach;?>
        </select>
    </div>
    <div class="contacts__items col-12 col-lg-6">

        <div class="c-filter__title">Выбрать клинику</div>

        <select class="select-lab labs-list" id="labs-list">
          <?foreach($arResult['ITEMS'] as $arLab):?>
            <option data-section-id="<?=$arLab['IBLOCK_SECTION_ID']?>" data-id="<?=$arLab['ID']?>" value="<?=$arLab['NAME']?>"><?=$arLab['NAME']?></option>
          <?endforeach;?>
        </select>

    </div>
</div>
<div class="contacts__map">
    <div class="map" id="maplabs"></div>
</div>

<script>
    $(document).ready(function(){
        App.labs.init({
            marker: '/local/templates/main/tpl/dist/assets/images/static/marker.svg',
            initial: 'г. Краснодар',
            initialLab: 'г. Краснодар, ул Урицкого,186',
            labs: <?=CUtil::PhpToJSObject($arResult['POINTS'])?>
        });
    });
</script>

<?endif;?>