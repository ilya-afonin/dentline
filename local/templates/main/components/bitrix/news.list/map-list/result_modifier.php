<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) { die(); }

// получаем разделы
$dbResSect = CIBlockSection::GetList(
    Array("SORT"=>"ASC"),
    Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'])
);
//Получаем разделы и собираем в массив
while($sectRes = $dbResSect->GetNext())
{
    $arSections[] = $sectRes;
}
//Собираем  массив из Разделов и элементов
foreach($arSections as $arSection){
    foreach($arResult["ITEMS"] as $key=>$arItem){
        if($arItem['IBLOCK_SECTION_ID'] == $arSection['ID']){
            $arSection['ELEMENTS'][] =  $arItem;
        }
    }
    $arElementGroups[] = $arSection;
}
$arResult["SECTIONS"] = $arElementGroups;


//Собираем метки
$arResult['POINTS'] = array();

foreach($arResult["SECTIONS"] as $arSec) {

  $labs = array();
  foreach ($arSec['ELEMENTS'] as $arItem){
    $labs[] = array(
        'id' => $arItem['ID'],
        'url' => $arItem['DETAIL_PAGE_URL'],
        'address' => $arItem['NAME'],
        'phone' => $arItem['PROPERTIES']['PHONE']['VALUE'],
        'work' => html_entity_decode($arItem['PROPERTIES']['WORK_TIME']['VALUE']['TEXT']),
        'coords' => explode(',', $arItem['PROPERTIES']['POINT']['VALUE'])
    );
  }

  $cities = array(
      'group' => $arSec['NAME'],
      'labs' => $labs
  );

  $arResult['POINTS'][] = $cities;

}