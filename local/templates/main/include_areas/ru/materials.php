<div class="materials s-container">
    <div class="materials__wrapper">
        <div class="materials__left">
            <div class="materials__title">расходные материалы для фрезерных центров</div>
            <div class="materials__text">Мы предлагаем широкий спектр расходных материалов для фрезерных центров.</div>
            <a class="materials__link" href="<?=SITE_TEMPLATE_PATH?>/tpl/dist/assets/files/Прайс2019 bloomnden.pdf" target="_blank" download>
                <img src="<?=SITE_TEMPLATE_PATH?>/tpl/dist/assets/images/static/materilas-ico.svg" alt="">
                Скачать прайс
            </a>
        </div>
        <div class="materials__right">
            <div class="materials__image">
                <img src="/local/templates/main/tpl/dist/assets/images/content/materials.png" alt="Материалы">
            </div>
        </div>
    </div>
</div>