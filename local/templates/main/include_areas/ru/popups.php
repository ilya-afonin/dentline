<div class="popups">

  <div class="popup popup--result" id="pop-zapis">
    <div class="popup__wrapper">
      <div class="popup__overlay"></div>
      <div class="popup__container">
        <div class="popup__closer"></div>
        <div class="popup__container-inner">
          <div class="popup__title">
            Запись на прием к врачу
            <div id="zapis_doctor"></div>
          </div>
          <? include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include_areas/' . LANGUAGE_ID . '/form_zapis.php'); ?>
        </div>
      </div>
    </div>
  </div>

</div>