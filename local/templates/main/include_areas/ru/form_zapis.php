<div class="form-wrapper form--zapis">
  <div class="form-header">
    <form class="form form--inline form-order js-validate" id="form-order">
      <input type="hidden" name="action" value="zapis">
      <input type="hidden" name="doctor" id="doctor" value="">
      <?= bitrix_sessid_post() ?>
      <div class="form__line form__line--group">
        <div class="form__input">
          <input class="form__input-field" type="text" name="name" placeholder="Представьтесь">
        </div>
        <div class="form__input">
          <input class="form__input-field mask--phone" type="text" name="phone" placeholder="Телефон">
        </div>
        <div class="form__input">
          <input class="form__input-field" type="text" name="email" placeholder="E-mail">
        </div>
      </div>
      <div class="form__line">
        <div class="form__input">
          <textarea class="form__input-field" name="message" placeholder="Может, поясните"></textarea>
        </div>
      </div>
      <div class="form__bottom">
        <button class="btn form__link" type="submit" role="button">Отправить</button>
      </div>
    </form>
  </div>

  <div class="form-ok">
    <div class="form-ok-left col-4">
      <div class="form-ok-img">
        <div class="col-11 offset-1"><svg width="112" height="100" viewBox="0 0 112 100" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M110.432 0.606219C110.152 0.656919 109.891 0.80209 109.7 1.01247C95.4295 15.9436 61.6153 52.2497 46.8535 67.7186L25.6878 49.0313C25.1897 48.5155 24.2465 48.5385 23.7742 49.078C23.3021 49.6177 23.4042 50.5556 23.9816 50.981L46.0816 70.481C46.579 70.9114 47.4129 70.8736 47.8691 70.4004C62.3326 55.2681 97.1436 17.8932 111.569 2.80037C112.331 2.0244 111.504 0.431402 110.432 0.606616V0.606219ZM0.134766 7.10622V98.106C0.134766 98.7867 0.754086 99.406 1.43477 99.406H92.4348C93.1155 99.406 93.7346 98.7867 93.7348 98.106V52.6063C93.7478 51.9195 93.1217 51.2876 92.4348 51.2876C91.7479 51.2876 91.125 51.9195 91.1348 52.6063V96.806H2.73477V8.40622H76.8348C77.5216 8.41532 78.1532 7.7931 78.1532 7.10622C78.1532 6.41934 77.5216 5.79651 76.8348 5.80622H1.43477C0.630456 5.80752 0.187286 6.48192 0.134766 7.10622Z" fill="white"/>
          </svg>

        </div>
      </div>
    </div>
    <div class="form-ok-right col-6 offset-1 offset-lg-2">
      <div class="form-ok-title">Спасибо за заявку!</div>
      <p class="form-ok-desc">В ближайщее время с Вами<br>свяжется менеджер</p>
    </div>
  </div>
</div>