<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$assets = \Bitrix\Main\Page\Asset::getInstance();
Loc::loadMessages(__FILE__);

?>
        <? if ($APPLICATION->GetProperty("show_sidebar") == 'Y'): ?>
                </div>
            </div>
        <?endif;?>
        </div>
<?if(!defined('PAGE') && PAGE != 'home'):?>
    </article>
<?endif;?>
</div>

<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__item"><? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/copyright.php", Array(), Array("MODE" => "html")); ?></div>
            <div class="footer__item"><a href="mailto:<?= \COption::GetOptionString("askaron.settings", "UF_MAIL"); ?>"><?= \COption::GetOptionString("askaron.settings", "UF_MAIL"); ?></a></div>
            <div class="footer__item"><a href="<?=$phone?>"><?= \COption::GetOptionString("askaron.settings", "UF_PHONE"); ?></a></div>
            <div class="footer__item footer__item--copyright">С любовью от &nbsp;<a href="https://de-us.ru" target="_blank">Deus</a></div>
        </div>
    </div>
</footer>

<? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/popups.php", Array(), Array("MODE" => "html")); ?>

</div>



<? if ((!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false) || !$USER->IsAdmin()):
    $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/yandex_metrika.php", Array(), Array("MODE" => "html"));
endif; ?>

<?
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/jquery-3.1.1.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/mobile-detect.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/iphone-inline-video.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/jquery.splitlines.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/jquery.mCustomScrollbar.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/smooth-scroll.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/smooth-scrollbar.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/ScrollMagic.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/jquery.ScrollMagic.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/validate.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/mask.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/youtube.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/owl.carousel.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/swiper.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/dropzone.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/jquery.countimator.min.js');
//$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/main.js');
if ($APPLICATION->GetCurDir() === '/contacts/' || $APPLICATION->GetCurDir() === '/stomatology/'){
    $assets->addJs("https://api-maps.yandex.ru/2.1/?apikey=526dba46-ed7b-460d-a85c-c13a4d60f43c&lang=ru_RU", true);
}
$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/lib/jquery-3.1.1.min.js');
$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/dist/assets/js/scripts.min.js');
?>
</body>
</html>
