$(document).ready(function() {

    App.labs = {
        data: null,
        initial: null,
        initialLab: null,
        marker: null,
        map: null,
        filterUrl: null,
        init: function(config) {
            App.labs.data = config['labs'];
            App.labs.initial = config['initial'];
            App.labs.initialLab = config['initialLab'];
            App.labs.marker = config['marker'];
            App.labs.filterUrl = config['filterUrl'];

            $('.select-lab').styler({
                onSelectClosed: App.labs.onSelectLab
            });

        },
        initMap: function() {
            App.labs.map = new ymaps.Map("maplabs", {
                center: [ 45.037938 , 38.975420 ],
                zoom: 11,
                controls: []
            });
            App.labs.map.controls.add('zoomControl', {right: '100%', top: '5px'});

            if (App.labs.initialLab) {
                ymaps.geocode(App.labs.initialLab)
                    .then(function (res) {
                        App.labs.map.panTo(res.geoObjects.get(0).geometry.getCoordinates());
                    });
            } else if (App.labs.initial) {
                ymaps.geocode(App.labs.initial)
                    .then(function (res) {
                        App.labs.map.panTo(res.geoObjects.get(0).geometry.getCoordinates());
                    });
            }
            App.labs.addLabs();
        },
        addLabs: function() {
            App.labs.map.geoObjects.removeAll();
            App.labs.data.forEach(function(group) {
                group['labs'].forEach(function(lab) {
                    var placemark = new ymaps.Placemark(
                        lab.coords.map(function(x){return parseFloat(x)}),
                        {
                            balloonContentHeader: lab.address,
                            balloonContentBody: '<p>Режим работы:</p> <span>' + lab.work + '</span>',
                        },
                        {
                            iconLayout: 'default#image' ,
                            iconImageHref: App.labs.marker,
                            iconImageSize: [14, 19]
                        }
                    );
                    placemark.properties.set('address', lab.address);
                    App.labs.map.geoObjects.add(placemark);
                });
            });
            if (App.labs.initial) {
                var tmp = $('#cities-list option[value="' + App.labs.initial + '"]');
                if (tmp.length) {
                    tmp.attr('selected', '');
                }
            }
            if (App.labs.initialLab) {
                var tmp = $('#labs-list option[value="' + App.labs.initialLab + '"]');
                if (tmp.length) {
                    tmp.attr('selected', '');
                }
                ymaps.geocode(App.labs.initialLab)
                    .then(function (res) {
                        App.labs.map.baloon.open(res.geoObjects.get(0).geometry.getCoordinates());
                    });
            }
            $('.select-lab').trigger('refresh');
        },
        onSelectLab: function() {
            var $that = $(this);
            if($that.hasClass('select-cities')){

                var city_id = $('#cities-list-styler .jq-selectbox__dropdown li.selected').data('id');

                $("select#labs-list option").addClass('hidden');
                var $labs = $("select#labs-list option[data-section-id='"+ city_id + "']");
                $labs.removeClass('hidden');
                if ($labs.eq(0).length) {
                    $labs.eq(0).attr('selected', '');
                }

                $('#labs-list').trigger('refresh');
                $(".select-lab").removeClass('hidden');

            }

            var value = $that.find('select').val();

            App.labs.map.geoObjects.each(function(element) {
                var address = element.properties.get('address');

                if (address.indexOf(value) > -1) {

                    App.labs.map.panTo(element.geometry.getCoordinates());
                    return false;
                }
            });

        }
    };
    ymaps.ready(App.labs.initMap);
});