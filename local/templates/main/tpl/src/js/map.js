$(window).on('load', function () {

    if ($('#map').length > 0) {

        ymaps.ready(initYaMap);
    }
    if ($('#mapOne').length > 0) {

        ymaps.ready(initOneMap);
    }
});

function getZoom() {

    if ($(window).width() > 1279) {
        return 16;
    }

    if ($(window).width() < 1280 && $(window).width() > 767) {
        return 15;
    }

    if ($(window).width() < 768) {
        return 14;
    }
}

function initOneMap() {

    //Карта с центром

    //var $map = $('#mapOne');

    var coords = $('#mapOne').data('points').split(',');

    var map = new ymaps.Map('mapOne', {
            center: coords,
            zoom: getZoom(),
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        }),
        placemark = new ymaps.Placemark(coords, {}, {
            // Опции.
            iconLayout: 'default#image',
            iconImageHref: '/local/templates/main/tpl/dist/assets/images/static/marker.svg',
            //iconImageSize: getMarkerSize(),
            //iconImageOffset: getMarkerOffset()
        });

    map.geoObjects.add(placemark);


}

function initYaMap() {

    //Общая карта
    if ($('#map').length > 0) {

        var $mapElement = $('#map');

        // Создание экземпляра карты.
        var myMap = new ymaps.Map($mapElement[0],
            ymaps.util.bounds.getCenterAndZoom(
                [[45.0, 38.9], [45.1, 39.0]],
                [$mapElement.width(), $mapElement.height()]
            ), {
                searchControlProvider: 'yandex#search'
            }),
            // Контейнер для меню.
            menu = $('<ul class="menu"></ul>');

        for (var i = 0, l = groups.length; i < l; i++) {
            //createMenuGroup(groups[i]);
        }

        function createMenuGroup(group) {
            // Пункт меню.
            var menuItem = $('<li><a href="#">' + group.name + '</a></li>'),
                // Коллекция для геообъектов группы.
                collection = new ymaps.GeoObjectCollection(null, {preset: group.style}),
                // Контейнер для подменю.
                submenu = $('<ul class="submenu"></ul>');

            // Добавляем коллекцию на карту.
            myMap.geoObjects.add(collection);
            // Добавляем подменю.
            menuItem
                .append(submenu)
                // Добавляем пункт в меню.
                .appendTo(menu)
                // По клику удаляем/добавляем коллекцию на карту и скрываем/отображаем подменю.
                .find('a')
                .bind('click', function () {
                    if (collection.getParent()) {
                        myMap.geoObjects.remove(collection);
                        submenu.hide();
                    } else {
                        myMap.geoObjects.add(collection);
                        submenu.show();
                    }
                });
            for (var j = 0, m = group.items.length; j < m; j++) {
                createSubMenu(group.items[j], collection, submenu);
            }
        }

        function createSubMenu(item, collection, submenu) {
            // Пункт подменю.
            var submenuItem = $('<li><a href="#">' + item.name + '</a></li>'),
                // Создаем метку.
                placemark = new ymaps.Placemark(item.center, {
                    // Зададим содержимое заголовка балуна.
                    balloonContentHeader: '<a href = "#">' + item.name + '</a><br>' +
                        '<span class="description">Сеть кинотеатров</span>',
                    // Зададим содержимое основной части балуна.
                    balloonContentBody: '<img src="img/cinema.jpg" height="150" width="200"> <br/> ' +
                        '<a href="tel:+7-123-456-78-90">+7 (123) 456-78-90</a><br/>' +
                        '<b>Ближайшие сеансы</b> <br/> Сеансов нет.',
                    // Зададим содержимое нижней части балуна.
                    balloonContentFooter: 'Информация предоставлена:<br/>OOO "Рога и копыта"',
                    // Зададим содержимое всплывающей подсказки.
                    hintContent: 'Рога и копыта'
                }, {
                    iconLayout: 'default#image',
                    iconImageHref: './assets/images/static/marker.svg',
                    iconImageSize: [18, 23],
                    iconImageOffset: [-9, -12]
                });

            // Добавляем метку в коллекцию.
            collection.add(placemark);
            // Добавляем пункт в подменю.
            submenuItem
                .appendTo(submenu)
                // При клике по пункту подменю открываем/закрываем баллун у метки.
                .find('a')
                .bind('click', function () {
                    if (!placemark.balloon.isOpen()) {
                        placemark.balloon.open();
                    } else {
                        placemark.balloon.close();
                    }
                    return false;
                });
        }

        // Добавляем меню в тэг BODY.
        //menu.appendTo($('body'));
        // Выставляем масштаб карты чтобы были видны все группы.
        myMap.setBounds(myMap.geoObjects.getBounds());

        myMap.behaviors.disable('scrollZoom');

        let winWidth = document.documentElement.clientWidth || document.body.clientWidth;
        if (winWidth > 1024) return false;
        ymapsTouchScroll(myMap);

    }
}
