let $pageDom = $('.page');
let contentHeight = $pageDom.height();
let scrollbar;
let customController;

App = {
    init: function (){
        initPage();
        headerScroll();
        headerMobile();

        formValidate();
        mask();
        dzInit();
        popups();
        mask();

        pSlider(); // основной на главной
        sSlider(); // спец предложения
        tSlider(); // команда, специалисты
        wSlider(); // выполненные работы
        fSlider(); // отзывы
        anchors();
        stickMenu();
        priceToggle();
        fancy();
        initWow();
    }
};

$(document).ready(function () {
    App.init();
});


const isMobile = () => {
    let e = new MobileDetect(window.navigator.userAgent);
    return e.mobile() || e.tablet()
};

const initPage = () => {

    initContent();

    setTimeout(function(){

        $(".preloader-line").css("width", $(".preloader-line").width()).removeClass("-loading").delay(0).queue(function (e) {
            $(this).css("width", "100vw");

            e();
        }).delay(640).queue(function (e) {

            if (isMobile || !$(".page").length){
                contentHeight = $pageDom.height();
                initScroller();
            }

            $('body').removeClass("-loading").addClass("-loaded"); 
            setTimeout(function () {
                $('body').removeClass("-loaded")
            }, 640);
            e()
        })
    }, 0);

}

const initContent = () => {

    initScrollbar();

    $('body').hasClass("-loading") || setTimeout(function () {
        !isMobile() && $pageDom.length ? contentHeight = scrollbar.size.content.height : contentHeight = $pageDom.height();
        initScroller();
    }, 1050);
}

const initScrollbar = () => {


    if (customController && customController.destroy(),
            customController = new ScrollMagic.Controller({
            refreshInterval: 20
        }), !isMobile() && $pageDom.length) {


        scrollbar = window.Scrollbar.init($pageDom[0]);

        scrollbar.addListener(function () {
            customController.update()
        });
        contentHeight = scrollbar.size.content.height;
    } else
        contentHeight = $pageDom.height();
}

const initScroller = () => {

    $("p.block__desc").each(function (e) {
        $(this).addClass('s-content__text').splitLines({
            tag: '<div class="line"><div class="line__content">'
        })
    });
    $("div.block__title").each(function (e) {
        $(this).addClass('s-content__text').splitLines({
            tag: '<div class="line"><div class="line__content">'
        })
    });

   /* $("div.person__edu").each(function (e) {
        $(this).addClass('s-content__text').splitLines({
            tag: '<div class="line"><div class="line__content">'
        })
    });*/

    $('.header, .block__image, .block__text, .s-content__text, .js-anchor, .form, .blank__block, .person__edu').each(function () {
        let t;
        if (isMobile())
            t = 1;
        else
            t = .8;
        new ScrollMagic.Scene({
            triggerElement: $(this),
            triggerHook: t,
            offset: 0
        }).setClassToggle($(this), "visible").addTo(customController)
    })

}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

const formValidate = () => {

    if ($('.js-validate').length > 0) {

        $('.js-validate').each(function () {

            $(this).validate({

                focusInvalid: true,
                highlight: function(element, errorClass, validClass) {
                    $(element).closest(".form__input").addClass(errorClass).removeClass(validClass);

                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).closest(".form__input").removeClass(errorClass).addClass(validClass);
                },

                rules: {

                    name: {
                        required: true
                    },

                    email: {
                        required: true,
                        email: true,
                        //regex: /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i
                    },

                    phone: {
                        required: true
                    },

                    blank: {
                        required: true
                    }

                },

                messages: {
                    name: '',
                    email: '',
                    phone: '',
                    blank: '',
                    privacy: ''
                },
                submitHandler: function (form) {

                    console.log('submit');

                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: '/local/tools/form-handler.php',
                        data: $(form).serialize(),
                        error: function () {
                            console.log("Error messages");
                        },
                        success: function (data) {
                            if (!data.error) {

                                $(form).find('input[type="text"]').value = '';
                                $(form).find('.form__error').fadeOut();

                                $(form).find(".dz-message .dz-remove").click();

                                //$('#pop-result').fadeIn(400);
                                //$('#pop-result').addClass('is-active');

                                $('input.c-form__input-field').each(function(){
                                    $(this).blur();
                                });

                                let isFormInline = $(form).hasClass('form--inline');
                                let formWrapper = $(form).closest('.form-wrapper');
                                let formHeader = formWrapper.find('.form-header');
                                let formOk = formWrapper.find('.form-ok');

                                if(isFormInline) {
                                    formOk.css('height', formHeader.height());
                                    formHeader.fadeOut(300, function () {
                                        $('.blank__block').hide();
                                        formOk.css('display', 'flex').hide().fadeIn();
                                    });

                                }

                                else {

                                    $('.popup__form').fadeOut(300, function () {

                                        $('.popup__success').fadeIn();
                                    });

                                    $('.popup').removeClass('popup--visible');

                                    $('body').removeClass('is-unscrolled');

                                }

                                //$(form).parents('.popup').addClass("is-submitted");

                                /*if (!!$(form).data("goal"))
                                    //yaCounter.reachGoal($(form).data("goal"));*/
                            }
                            else {
                                $(form).find('.form__error').text(data.message);
                                $(form).find('.form__error').fadeIn();
                            }
                        }
                    });

                    $(".mask--phone").mask('+7(999)-999-99-99',{placeholder:" "});

                    return false;

                }
            });
        });
    }
}

const mask = () => {

    if ($('.mask--phone').length > 0) {

        $(".mask--phone").mask('+7(999)-999-99-99');

    }
}

const dzInit = () => {

    if ($('.dZUpload').length > 0) {

        $('.dZUpload').each(function(){

            $(this).dropzone({

                url: "/local/tools/upload_main.php",

                addRemoveLinks: true, //add remove link

                dictRemoveFile: '+', //remove link text

                dictCancelUpload: 'Cancel upload',

                dictMaxFilesExceeded: 'Вы не можете загружать больше файлов. Максимальное количество файлов: {{maxFiles}}',
                dictDefaultMessage: "Перетащите сюда файлы для загрузки",
                dictFallbackMessage: "Ваш браузер не поддерживает перетаскивание файлов.",
                dictFallbackText: "Пожалуйста, используйте резервную форму ниже, чтобы загрузить файлы, как в старые времена.",
                dictFileTooBig: "Файл слишком большой ({{filesize}}MiB). Максимальный размер файла: {{maxFilesize}}MiB.",
                dictInvalidFileType: "Вы не можете загружать файлы подобного типа.",
                dictResponseError: "Сервер ответил кодом {{statusCode}}.",
                dictCancelUploadConfirmation: "Вы уверены, что хотите отменить эту загрузку?",

                maxFiles: 1,
                maxFilesize: 20,
                init: function () {
                    let myDropZone = this;
                    let $message = $('.dz-message');
                    let uploadFilesField = $('input[name="uploaded_files"]');

                    this.on("success", function (file, response) {
                        if (response == 1) {
                            $('#input_filename').val(file.name);
                            //$message.append(file._removeLink);
                            $message.closest('.form__line').find('.form__input').eq(0).addClass('valid');
                            $('.dz-icon').css('display', 'none');

                            /*$('.dz-preview').prepend('<p class="complete"> File successfully uploaded</p>');*/

                            uploadFilesField.val(file['name']);

                        } else {
                            /*$('.dz-preview').prepend('<p class="complete error">Error loading file!</p>');*/
                        }

                        $(".dz-remove").click(function (e) {
                            e.preventDefault();
                            $message.find('.dz-remove').remove();
                            $('.dz-icon').css('display', 'block');
                            $message.find('span').text('Выберите файл');
                            $message.closest('.c-form__input').removeClass('is-full');
                            uploadFilesField.value = '';
                            myDropZone.removeAllFiles();
                        });
                    }),
                    this.on("error", function (file, message) {
                        $(file.previewElement).prepend('<p class="complete error">Error loading file! ' + message + '</p>');
                    })
                }
            });
        });
    }
}

const popups = () => {

    if ($('.popup').length > 0) {

        $('body').on('click', '[data-popup]', function () {

            var needPopup = $(this).attr('data-popup');


            if($(this).attr('data-person').length > 0){
                console.log($(this).attr('data-person'));
                let doctor = $(this).attr('data-person');
                let doctor_id = $(this).attr('data-person-id');
                $('#zapis_doctor').text(doctor);
                $('#doctor').val(doctor_id);
            }

            $('#' + needPopup).addClass('popup--visible');

            $('body').addClass('is-unscrolled');

            return false;
        });

        $('body').on('click', '.popup__closer', function () {

            $('.popup').removeClass('popup--visible');

            $('body').removeClass('is-unscrolled');

        });

        $(document).keyup(function (e) {

            if (e.keyCode === 27) {

                $('.popup').removeClass('popup--visible');

                $('body').removeClass('is-unscrolled');
            }
        });

        $(document).on('click', function (e) {

            var block = $('.popup__container');

            if (!block.is(e.target) &&
                block.has(e.target).length === 0 && $('.popup').hasClass('popup--visible')) {

                $('.popup').removeClass('popup--visible');

                $('body').removeClass('is-unscrolled');
            }
        });
    }
}

const headerScroll = () => {

    if ($('.header').length > 0) {

        let currentPosition = $(window).scrollTop();
        let lastPosition = 0;
        let header = $('.header');

        let condition = true;

        $(window).on('scroll', function () {

            currentPosition = $(window).scrollTop();

            if (currentPosition > 192  && !condition) {

                if(!header.hasClass('is-hidden'))
                    header.addClass('is-hidden');
            }

            if (currentPosition < 0) {
                return false;
            }

            else if (currentPosition < 82) {

                if(header.hasClass('is-scrolled'))
                    header.removeClass('is-scrolled');

            } else if (currentPosition > lastPosition && condition) {

                header.addClass('is-scrolled');

                condition = false;

            } else if (currentPosition < lastPosition && (currentPosition == 192  || currentPosition === lastPosition)) {


                header.removeClass('is-hidden');

                condition = true;
            }
            else if (currentPosition < lastPosition && currentPosition > 192 && !condition) {

                header.removeClass('is-hidden');

                condition = true;

            }

            lastPosition = currentPosition;
        });

    }
}

const headerMobile = () => {

    $('body').on('click', '.h__burger', function () {

        $(this).toggleClass('is-active');
        $('.h__row').toggleClass('is-opened');
        $('body').toggleClass('is-unscrolled');
    });

    $('body').on('click', '.h__nav-link', function () {
        if ($('body').hasClass('is-unscrolled'))
            $('body').removeClass('is-unscrolled');
    });
}

const pSlider = () => {

    if ($('.p-slider').length > 0) {

        $('.p-slider').each(function () {

            let slider = $(this);
            let scene = slider.find('.p-slider__scene');

            let dotsDom = slider.find('.p-slider__controls-inner');

            scene.owlCarousel({
                items: 1,
                autoplay: true,
                autoplayHoverPause: true,
                smartSpeed: 800,
                //animateIn: 'fadeIn',
                //animateOut: 'fadeOut',
                loop: true,
                nav: false,
                dots: true,
                dotsContainer: dotsDom,

            });
        });
    }
};

const sSlider = () => {
    if ($('.s-slider').length > 0) {

        $('.s-slider').each(function () {

            let slider = $(this);
            let scene = slider.find('.s-slider__scene');
            let dotsDom = slider.find('.p-slider__controls-inner');
            let navigation = $(this).parents('.s-container').find('.s-navigation');

            scene.owlCarousel({
                items: 2,
                loop: false,
                /*animateIn: 'fadeIn',
                animateOut: 'fadeOut',*/
                nav: true,
                dots: false,
                margin: 30,
                navContainer: navigation,
                navText: ['Назад', 'Вперед'],
                navElement: 'a',
                navContainerClass: 's-arrows',
                navClass: ['s-arrows__arrow s-arrows__arrow--prev', 's-arrows__arrow s-arrows__arrow--next'],
                responsive: {
                    0: {
                        items: 1,
                        mouseDrag: true,

                        dots: true,
                        dotsContainer: dotsDom,
                        center: true,
                    },
                    768: {

                    },
                    1024: {
                        items: 2,
                        mouseDrag: false,
                        dots: false,
                        nav: false,
                    },
                    1200: {
                        center: false,
                    },
                }
            });
        });
    }
}

const tSlider = () => {
    if ($('.t-slider').length > 0) {
        let slider = $('.t-slider__scene');
        slider.each(function () {
            let navigation = $(this).parents('.t-container').find('.s-navigation');
            let owl = $(this);
            owl.owlCarousel({
                items: 4,
                loop: false,
                nav: true,
                dots: false,
                mouseDrag: false,
                autoWidth: true,
                margin: 30,
                navContainer: navigation,
                navText: ['Назад', 'Вперед'],
                navElement: 'a',
                navContainerClass: 's-arrows',
                navClass: ['s-arrows__arrow s-arrows__arrow--prev', 's-arrows__arrow s-arrows__arrow--next'],
                responsive: {
                    0: {
                        items: 2,
                        mouseDrag: true,
                        margin: 10
                    },
                    768: {
                        items: 3.4,
                    },
                    1024: {
                        margin: 20,
                        mouseDrag: false,
                    },
                    1200: {
                        center: false,
                        margin: 30
                    },
                }
            });
        });
    }
}

const wSlider = () => {
    if ($('.w-slider').length > 0) {
        let slider = $('.w-slider__scene');
        slider.each(function () {
            let navigation = $(this).parents('.w-container').find('.s-navigation');
            let owl = $(this);
            owl.owlCarousel({
                items: 3,
                loop: false,
                nav: true,
                dots: false,
                mouseDrag: false,
                autoWidth: true,
                margin: 30,
                navContainer: navigation,
                navText: ['Назад', 'Вперед'],
                navElement: 'a',
                navContainerClass: 's-arrows',
                navClass: ['s-arrows__arrow s-arrows__arrow--prev', 's-arrows__arrow s-arrows__arrow--next'],
                responsive: {
                    0: {
                        items: 1.4,
                        mouseDrag: true,
                        margin: 10
                    },
                    768: {
                        items: 3.4,
                    },
                    1024: {
                        margin: 20,
                        mouseDrag: false,
                    },
                    1200: {
                        margin: 30,
                        center: false,
                    },
                }
            });
        });
    }
}

const fSlider = () => {
    if ($('.f-slider').length > 0) {
        let slider = $('.f-slider__scene');
        slider.each(function () {
            let navigation = $(this).parents('.w-container').find('.s-navigation');
            let owl = $(this);
            owl.owlCarousel({
                items: 1,
                loop: false,
                nav: true,
                dots: false,
                mouseDrag: false,
                navContainer: navigation,
                navText: ['Назад', 'Вперед'],
                navElement: 'a',
                navContainerClass: 's-arrows',
                navClass: ['s-arrows__arrow s-arrows__arrow--prev', 's-arrows__arrow s-arrows__arrow--next'],
                responsive: {
                    0: {
                        mouseDrag: true,
                    },
                    1024: {
                        mouseDrag: false,
                    },
                }
            });
        });
    }
}

const anchors = () => {

    $('[data-anchor]').on('click', function () {

        let index = $(this).attr('data-anchor');

        if(index.indexOf('/') >= 0){
            let links_arr =  index.split('/');
            index = links_arr.pop();
        }

        let offset = $('#' + index).offset().top - 60;

        $('.sidenav__item').removeClass('sidenav__item--active');

        $(this).parent('.sidenav__item').addClass('sidenav__item--active');

        $('html, body').animate({
            scrollTop: offset
        }, 700);

        $('.header__burger').removeClass('is-active');
        $('.nav').removeClass('is-opened');
        $('.header').removeClass('is-opened');

        return false;
    });

    $(window).on('scroll load', function () {

        let scrolled = $(window).scrollTop();

        if (scrolled >= 50) {
            $('.js-anchor').each(function (i) {
                if ($(this).position().top <= scrolled + 100 && scrolled < $(this).position().top + $(this).innerHeight() + 100) {
                    let needId = $(this).attr('id');
                    $('.sidenav__item').removeClass('sidenav__item--active');
                    $('.sidenav__item').eq(i).addClass('sidenav__item--active');
                }
            });
        }
    });
};

const stickMenu = () => {

    if ( $('.is-sticky').length > 0 ) {

        $('.is-sticky').each(function () {

            let that = $(this);

            let thatOffset = that.offset().top;



            $(window).on('scroll', function () {

                let thatStop = that.parent().innerHeight() + that.parent().offset().top;

                let resolution = $(window).width();

                if (resolution >= 1024) {

                    let scrolled = $(window).scrollTop();

                    let thatOffsetBottom = that.offset().top + that.innerHeight();

                    if (scrolled + 130 < thatOffset) {

                        that.removeClass('is-fixed');
                    }

                    if ( (scrolled + 130 >= that.parent().offset().top) && (scrolled + 130 < that.parent().offset().top + that.parent().innerHeight() - 200) ) {

                        if (that.hasClass('is-bottom')) {

                            if (scrolled + 130 <= that.offset().top) {

                                that.removeClass('is-bottom');
                            }

                        } else {

                            if (scrolled + 130 >= that.parent().offset().top && thatOffsetBottom < thatStop) {

                                that.addClass('is-fixed');

                            } else {

                                that.addClass('is-bottom');
                            }
                        }
                    }
                }
            });
        });
    }
}

const priceToggle = () => {

    if ($('.prices').length === 0) return false;

    $('.price__item').on('click', function () {

        $(this).toggleClass('is-active');
        $(this).find('.price__list').slideToggle();
    });
}

const fancy = () => {

    if ($('[data-fancybox]').length > 0) {

        $('[data-fancybox]').fancybox({
            buttons: [
                "close"
            ],
            helpers: {
                media: true
            },
            youtube: {
                autoplay: 1,
            },
            beforeShow: function () {
                $(".page").addClass("blur");
            },
            afterClose: function () {
                $(".page").removeClass("blur");
            }
        });
    }
}

const initWow = () => {


    new WOW().init();

}
