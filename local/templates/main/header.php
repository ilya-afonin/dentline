<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Deus\Helpers\Main;

$assets = \Bitrix\Main\Page\Asset::getInstance();
Loc::loadMessages(__FILE__);
?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID; ?>">
<head>
    <title><? $APPLICATION->ShowTitle(false); ?></title>
    <meta charset="<?= LANG_CHARSET; ?>">
    <?php $APPLICATION->ShowMeta("keywords"); ?>
    <?php $APPLICATION->ShowMeta("description"); ?>
    <?php $APPLICATION->ShowMeta("robots"); ?>
    <meta http-equiv="X-UA-Compatible&quot;,content=&quot;IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php /*<link rel="shortcut icon" href="/favicon.ico">*/ ?>
    <?
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/animate.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/fancybox.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/lightbox.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/mcustomscrollbar.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/owl.carousel.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/youtube.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/dropzone.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/odometer-theme-default.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/lib/swiper.min.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/main.css");
    //    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/media.css");
    $assets->addCss(SITE_TEMPLATE_PATH . "/tpl/dist/assets/css/styles.css");

    $APPLICATION->ShowCSS();
    $APPLICATION->ShowHeadStrings();
    ?>
</head>


<body class="-loading -page-<?= PAGE ?>">
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>

<div class="page main">
    <header class="header">
        <div class="container">
            <div class="h__row">
                <? $phone = Main::getPhoneLink(\COption::GetOptionString("askaron.settings", "UF_PHONE")); ?>
                <div class="h__mail-wrapper">
                    <a class="h__mail-mobile" href="mailto:<?= \COption::GetOptionString("askaron.settings", "UF_MAIL") ?>"></a>
                    <a class="h__mail"
                       href="mailto:<?= \COption::GetOptionString("askaron.settings", "UF_MAIL") ?>"><?= \COption::GetOptionString("askaron.settings", "UF_MAIL") ?></a>
                </div>
                <div class="h__logo-wrapper">
                    <a class="h__logo" href="/">
                        <? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/logo.php", Array(), Array("MODE" => "html")); ?>
                    </a>
                </div>
                <div class="h__phone-wrapper">
                    <a class="h__phone-mobile" href="<?= $phone ?>"></a>
                    <a class="h__phone" href="<?= $phone ?>">
                        <?= \COption::GetOptionString("askaron.settings", "UF_PHONE") ?>
                    </a>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top",
                    array(
                        "ROOT_MENU_TYPE" => "top",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "COMPONENT_TEMPLATE" => "top"
                    ),
                    false
                );
                ?>

                <div class="h__burger">
                    <div class="h__burger-line"></div>
                    <div class="h__burger-line"></div>
                </div>
            </div>
        </div>
    </header>
    <div class="preloader">
        <div class="preloader__container">
            <div class="preloader__line preloader-line -loading"></div>
        </div>
    </div>
    <div class="content">
        <?if(PAGE != 'home'):?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "main",
                Array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0"
                )
            ); ?>
        <?endif;?>
        <?if(!defined('PAGE') && PAGE != 'home'):?>
        <article class="user-content-styles">
            <?endif;?>
            <div class="container">
                <? if ($APPLICATION->GetProperty("show_sidebar") == 'Y'): ?>
                <div class="content-block row">
                    <aside class="left__menu hide-for-medium-down col-md-3">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "left",
                            array(
                                "ROOT_MENU_TYPE" => "left",
                                "MENU_CACHE_TYPE" => "Y",
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "MENU_CACHE_GET_VARS" => array(),
                                "MAX_LEVEL" => "1",
                                "CHILD_MENU_TYPE" => "",
                                "USE_EXT" => "N",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "N",
                                "COMPONENT_TEMPLATE" => "left"
                            ),
                            false
                        );
                        ?>
                    </aside>
                    <div class="page-content col-12 col-md-9">
                    <? endif; ?>
