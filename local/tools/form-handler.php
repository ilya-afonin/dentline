<?
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");



function blank() {

  if (check_bitrix_sessid()) {
      //$email_pattern = "/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|\"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?\")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i";
      $email_pattern = "~^([a-z0-9_\-\.])+@([a-z0-9_\-\.])+\.([a-z0-9])+$~i";
    if (!preg_match($email_pattern, $_REQUEST["email"]) && $_REQUEST["email"] != "") {
      $EXIT["error"] = true;
      $EXIT["message"] = "Неверный email";
      echo json_encode($EXIT);
      die();
    }

    $EXIT["error"] = false;
    CModule::IncludeModule("iblock");

    $target2 = array();
    if (isset($_REQUEST["uploaded_files"]) && !empty($_REQUEST["uploaded_files"])) {
      $i = 0;

      $tar = explode("|", $_REQUEST["uploaded_files"]);
      foreach ($tar as $n) {
        if ($n == "") {
          continue;
        }

        $info = pathinfo($n);
        $ext = $info['extension']; // get the extension of the file
        $newname = md5($n . $_SESSION["KEY"]) . "." . $ext;
        $target2[] = '/upload/mails/' . $newname;
        $i++;
      }
    }

    $name = "Новый заказ от - ".date('d.m.Y H:i');
    $subject = "Добавлен новый бланк заказа от ".date('d.m.Y H:i');

    $arFiles = array();
    foreach($target2 as $file) {
      $arFiles[] = CFile::MakeFileArray($file);
    }

    $prop = array();

    $prop[11] = $_REQUEST["name"];
    $prop[12] = $_REQUEST["email"];
    $prop[13] = $_REQUEST["phone"];
    $prop[14] = $_REQUEST["message"];
    $prop[15] = $arFiles;


    $arStory = Array(
        "IBLOCK_ID" => 7,
        "PROPERTY_VALUES" => $prop,
        "NAME" => $name,
        "DATE_ACTIVE_FROM"=>ConvertTimeStamp(time(), "FULL"),
        "ACTIVE" => "Y",
    );

    $el = new CIBlockElement;

    if ($elID = $el->Add($arStory)) {

      $arFields = Array(
          "AUTHOR" => $_REQUEST["name"],
          "AUTHOR_EMAIL" => $_REQUEST["email"],
          "TEXT" => $_REQUEST["message"],
          "TEL" => $_REQUEST["phone"],
          "SUBJECT" => $subject,
      );

      if (!CEvent::Send("FEEDBACK_FORM", SITE_ID, $arFields, "N", 7, $target2)){
        $EXIT['error'] = true;
      }
      else
        $EXIT["message"] = "Ваше сообщение успешно отправлено!";
    } else {
      $EXIT['error'] = true;
    }

  }
  echo json_encode($EXIT);
}

function callback() {

  if (check_bitrix_sessid()) {
      //$email_pattern = "/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|\"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?\")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i";
      $email_pattern = "~^([a-z0-9_\-\.])+@([a-z0-9_\-\.])+\.([a-z0-9])+$~i";
    if (!preg_match($email_pattern, $_REQUEST["email"]) && $_REQUEST["email"] != "") {
      $EXIT["error"] = true;
      $EXIT["message"] = "Неверный email";
      echo json_encode($EXIT);
      die();
    }

    $EXIT["error"] = false;
    CModule::IncludeModule("iblock");

    $name = "Обратный звонок от - ".date('d.m.Y H:i');
    $subject = "Заявка на обратный звонок от ".date('d.m.Y H:i');

    $prop = array();

    $prop[20] = $_REQUEST["phone"];


    $arStory = Array(
        "IBLOCK_ID" => 9,
        "PROPERTY_VALUES" => $prop,
        "NAME" => $name,
        "DATE_ACTIVE_FROM"=>ConvertTimeStamp(time(), "FULL"),
        "ACTIVE" => "Y",
    );

    $el = new CIBlockElement;

    if ($elID = $el->Add($arStory)) {

      $arFields = Array(
          "PHONE" => $_REQUEST["phone"],
          "SUBJECT" => $subject
      );

      if (!CEvent::Send("FEEDBACK_FORM", SITE_ID, $arFields, "N", 9)){
        $EXIT['error'] = true;
      }
      else
        $EXIT["message"] = "Ваше сообщение успешно отправлено!";
    } else {
      $EXIT['error'] = true;
    }

  }
  echo json_encode($EXIT);
}

function zapis() {

  if (check_bitrix_sessid()) {
      //$email_pattern = "/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|\"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?\")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i";
      $email_pattern = "~^([a-z0-9_\-\.])+@([a-z0-9_\-\.])+\.([a-z0-9])+$~i";
    if (!preg_match($email_pattern, $_REQUEST["email"]) && $_REQUEST["email"] != "") {
      $EXIT["error"] = true;
      $EXIT["message"] = "Неверный email";
      echo json_encode($EXIT);
      die();
    }

    $EXIT["error"] = false;
    CModule::IncludeModule("iblock");

    $name = "Запись на прием от - ".date('d.m.Y H:i');
    $subject = "Запись на прием от ".date('d.m.Y H:i');

    $prop = array();

    $prop[16] = $_REQUEST["name"];
    $prop[17] = $_REQUEST["phone"];
    $prop[18] = $_REQUEST["email"];
    $prop[19] = $_REQUEST["message"];
    $prop[35] = $_REQUEST["doctor"];


    $arStory = Array(
        "IBLOCK_ID" => 8,
        "PROPERTY_VALUES" => $prop,
        "NAME" => $name,
        "DATE_ACTIVE_FROM"=>ConvertTimeStamp(time(), "FULL"),
        "ACTIVE" => "Y",
    );

    $el = new CIBlockElement;

    if ($elID = $el->Add($arStory)) {

      $arFields = Array(
          "AUTHOR" => $_REQUEST["name"],
          "AUTHOR_EMAIL" => $_REQUEST["email"],
          "TEL" => $_REQUEST["phone"],
          "TEXT" => $_REQUEST["message"],
          "SUBJECT" => $subject,
      );

      if (!CEvent::Send("FEEDBACK_FORM", SITE_ID, $arFields, "N", 7)){
        $EXIT['error'] = true;
      }
      else
        $EXIT["message"] = "Ваше сообщение успешно отправлено!";
    } else {
      $EXIT['error'] = true;
    }

  }
  echo json_encode($EXIT);
}

switch ($_REQUEST["action"]) {
  case "blank":
    blank();
    break;
  case "callback":
    callback();
    break;
  case "zapis":
    zapis();
    break;
}