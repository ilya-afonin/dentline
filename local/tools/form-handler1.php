<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Web\HttpClient,
    Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Request;

  $request = Context::getCurrent()->getRequest();

  $action = $request->getPost("action");

  $name = $request->getPost("Name");
  $email = $request->getPost("Email");
  $phone = $request->getPost("Phone");
  $comment = $request->getPost("Comment");
  $doctor = $request->getPost("Doctor");
  $disease = $request->getPost("disease");


if ($name && $email && $phone) {

  CModule::IncludeModule("iblock");

  $answer = array();

  $answer["error"] = false;


  if (!preg_match("~^([a-z0-9_\-\.])+@([a-z0-9_\-\.])+\.([a-z0-9])+$~i", $email) && $email != "") {
    $answer["error"] = true;
    $answer["message"] = "Неверный email";
    echo json_encode($answer);
    die();
  }

  $doctor = ($doctor)? ' '.$doctor:'';
  $type = 5;

  $subject = "Запись".$doctor." с сайта happylook.ru";

  switch ($action){
    case 'manufacturing':
      $subject = 'Запись на изготовление очков';
      $type = 1;
      break;
    case 'diagnostics':
      $subject = 'Запись на диагностику';
      $type = 2;
      break;
    case 'freelesson':
      $subject = 'Запись на бесплатное занятие: ' . $disease;
      $type = 3;
      break;
    case 'order':
      $subject = "Запись". $doctor. " с сайта happylook.ru";
      $type = 4;
      break;
    default:
      $subject = 'Запись с сайта happylook.ru';
      $type = 5;
      break;
  }


  $prop = array();

  $prop[22] = $type;
  $prop[23] = $name;
  $prop[24] = $email;
  $prop[25] = $phone;

  $arElems = Array(
      "IBLOCK_ID" => 12,
      "PROPERTY_VALUES" => $prop,
      "NAME" => $subject,
      "DETAIL_TEXT" => htmlspecialchars_decode($comment),
      "DATE_ACTIVE_FROM"=>ConvertTimeStamp(time(), "FULL"),
      "ACTIVE" => "Y",
  );

  $el = new CIBlockElement;

  if ($elID = $el->Add($arElems)) {
    $arEventFields = array(
        "NAME" => $name,
        "EMAIL" => $email,
        "PHONE" => $phone,
        "COMMENT" => $comment,
        "SUBJECT" => $subject,
        "DATE" => ConvertTimeStamp(time(), "FULL")

    );

    if (!CEvent::Send("FEEDBACK_FORM", SITE_ID, $arEventFields)){
      $answer['error'] = true;
    } else
      $answer["message"] = "Ваше сообщение успешно отправлено!";

  } else {
    $answer['error'] = true;
  }

} else
  $answer["error"] = true;

echo json_encode($answer);
