<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
Loc::loadMessages(__FILE__);
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (!empty($arResult["ITEMS"])):
			echo "<pre>";
				//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/temp.txt", var_export($arResult["ITEMS"],1).PHP_EOL, FILE_APPEND);
			echo "</pre>";

?>
    <script type="text/javascript">
        $(function () {
            $('.infobox__icons/*, .map-info__categories*/').tooltip({
                tooltipClass: "infobox-tooltip"
            });
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.4/bluebird.min.js"></script>
  <?
    if (!empty($arParams['AJAX_ID'])):?>
        <style>
            #wait_comp_<?=$arParams['AJAX_ID']?> {
                display: none;
            }
        </style>
    <?endif; ?>
    <div class="map-info__categories">
		<?$bIsFalse = false;?>
        <? foreach ($arResult['CATEGORIES'] as $category):
			echo "<pre>";
				//file_put_contents($_SERVER["DOCUMENT_ROOT"]."/temp2.txt", var_export($arResult['CATEGORIES'],1).PHP_EOL, FILE_APPEND);
			echo "</pre>";
            $selectedCategory = !empty($arParams['CATEGORY']) ? $arParams['CATEGORY'] : array();
            $index = array_search($category['ID'], $selectedCategory);

            $strParams = '';
            $class = '';
            if ($index === false) {
                $selectedCategory[] = $category['ID'];

				if(in_array(664,$selectedCategory))
				{
					if($_REQUEST["category"] == 664)
						$bIsFalse = true;
					unset($selectedCategory);
					$selectedCategory[] = $category['ID'];
				}
            } else {	
					$class = ' is-active';
                unset($selectedCategory[$index]);
            }
            if (count($selectedCategory) > 0) {
                  $strParams = 'category=' . implode(':', $selectedCategory);
            }

            ?>
            <a href="<?= $APPLICATION->GetCurPageParam($strParams, array('category')); ?>"
               style="background-image: url(<?= $category['ICON']['SRC'] ?>)" class="map-info__category<?= $class ?>"
               title="<?= $category['NAME']; ?>"
               data-id="<?= $category['ID']?>"></a>
        <? endforeach; ?>
    </div>
	<?if($bIsFalse):?>
		<?
		$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_CITY");
		$arFilter = Array("IBLOCK_ID"=>7, "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$field = $ob->GetFields();
			$prop = $ob->GetProperties();
			$arSityes[] = $prop["CITY"]["VALUE"];
		}
		?> 
		<div class="test">
			<input class="spisok-gorodov-input" id="searchInput" type="text" placeholder="Выберите город..." autocomplete="off" value="<?=$_REQUEST["ID_TO_SHOW"]?>"/>
			<div class="clos-x">X</div>
			<div class="test2" id="list" name="" size="">
				<span>Выберите город</span>
				<?
					$arDubli = array();
					sort($arSityes);
					foreach($arSityes as $arItem)
					{
						if(!in_array($arItem, $arDubli))
						{
							$arDubli[] = $arItem;
							?><span class="opt_txt" <?=($_REQUEST["ID_TO_SHOW"] == $arItem) ? "selected" : "";?>><?=$arItem?></span><?
						}
					}
				?>
			</div>
		</div>
	<?endif;?>
	

	
	
	
    <div class="map-info__cities">
		<? 		
		foreach ($arResult["ITEMS"] as $arItem): 
				
				if(isset($_REQUEST["ID_TO_SHOW"]) && strlen($_REQUEST["ID_TO_SHOW"])>2)
					if(strripos($arItem['DISPLAY_PROPERTIES']['CITY']['VALUE'], $_REQUEST["ID_TO_SHOW"]) === false) 
					continue;
?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

            ?>
            <div class="map-info__city">
                <a id="<?= $this->GetEditAreaId($arItem['ID']); ?>" href="#"
                   class="map-info__city-title"><?= $arItem['DISPLAY_PROPERTIES']['CITY']['VALUE'] ?></a>
                <div class="map-info__city-text"><?= $arItem['NAME'] ?></div>
                <?
				
                if (!empty($arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'])): ?>
                    <? foreach ((Array)$arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'] as $phone): ?>
                        <a href="tel:<?= str_replace(array(
                            ' ',
                            '(',
                            ')',
                            '-'
                        ), '', $phone) ?>" class="map-info__city-contact"><?= $phone ?></a>
                    <? endforeach; ?>
                <? endif; ?>
				
				<? if (!empty($arItem['DISPLAY_PROPERTIES']['PARTNER_SITE']['VALUE'])): ?>
                    <? foreach ((Array)$arItem['DISPLAY_PROPERTIES']['PARTNER_SITE']['VALUE'] as $p_site): ?>
                        <a href="<?=$p_site ?>" class="map-info__partner_site"><?=$p_site ?></a>
                    <? endforeach; ?>
                <? endif; ?>
				
				<? if (!empty($arItem["PROPERTIES"]["PARTNER_SITE"]["VALUE"])): ?>
                    <? foreach ((Array)$arItem["PROPERTIES"]["PARTNER_SITE"]["VALUE"] as $p_site): ?>
                        <a href="<?=$p_site ?>" target="_blank" class="map-info__partner_site"><?=$p_site ?></a>
                    <? endforeach; ?>
                <? endif; ?>
				
                <? if (!empty($arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])): ?>
                    <? foreach ((Array)$arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'] as $mail): ?>
                        <a href="tel:<?= $mail ?>" class="map-info__city-contact"><?= $mail ?></a>
                    <? endforeach; ?>
                <? endif; ?>

            </div>
        <? endforeach; ?>
	</div>

<div <?if(isset($_REQUEST["ID_TO_SHOW"])): $points=explode(",",$arItem["PROPERTY_19"]);?>data-lng="<?=$points[0]?>" data-lat="<?=$points[1]?>"<?endif;?> class="infoboxes"><? foreach ($arResult["ITEMS"] as $arItem): ?>
            <div id="point_<?= $arItem['ID'] ?>" class="infobox point_<?= $arItem['ID'] ?>">
                <a href="" class="infobox-btn-close infobox-btn-close--region infobox_close_<?= $arItem['ID'] ?>"><span></span><span></span></a>
                <div class="infobox-txt">
                    <div class="infobox__title"><?= $arItem['DISPLAY_PROPERTIES']['CITY']['VALUE'] ?></div>
                    <div class="infobox__subtitle"><?= $arItem['NAME'] ?></div>
                    <div class="infobox__addr">
                        <div class="infobox__addr-title">Адрес:</div>
                        <div class="infobox__addr-text"><?= $arItem['DISPLAY_PROPERTIES']['ADDRESS']['VALUE'] ?></div>
                    </div>
                    <div class="infobox__person">
                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['CONTACT_NAME']['VALUE'])): ?>
                            <div
                                class="infobox__person-name"><?= $arItem['DISPLAY_PROPERTIES']['CONTACT_NAME']['VALUE'] ?></div>
                        <? endif; ?>
                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['POST']['VALUE'])): ?>
                            <div
                                class="infobox__person-position"><?= $arItem['DISPLAY_PROPERTIES']['POST']['VALUE'] ?></div>
                        <? endif ?>
                    </div>
                    <div class="infobox__contacts">
                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'])): ?>
                            <div class="map-info__city-contact">
                                <? foreach ((Array)$arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'] as $phone): ?>
                                    <div class="map__contact">
                                      <a href="tel:<?= str_replace(array(
                                          ' ',
                                          '(',
                                          ')',
                                          '-'
                                      ), '', $phone) ?>"><?= $phone ?></a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
						
						<? if (!empty($arItem['DISPLAY_PROPERTIES']['PARTNER_SITE']['VALUE'])): ?>
							<? foreach ((Array)$arItem['DISPLAY_PROPERTIES']['PARTNER_SITE']['VALUE'] as $p_site): ?>
								<div><a href="<?=$p_site ?>" class="map-info__partner_site mp"><?=$p_site ?></a></div>
							<? endforeach; ?>
						<? endif; ?>
						
						<? if (!empty($arItem["PROPERTIES"]["PARTNER_SITE"]["VALUE"])): ?>
							<? foreach ((Array)$arItem["PROPERTIES"]["PARTNER_SITE"]["VALUE"] as $p_site): ?>
								<div><a href="<?=$p_site ?>" target="_blank" class="map-info__partner_site mp"><?=$p_site ?></a></div>
							<? endforeach; ?>
						<? endif; ?>
				
                        <? if (!empty($arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])): ?>
                            <div class="map-info__city-contact">
                                <? foreach ($arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'] as $mail): ?>
                                    <a href="mailto:<?= $mail ?>"><?= $mail ?></a>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
                      <? if (!empty($arItem['PROPERTIES']['SERVICES']['VALUE'])): ?>
                        <div class="infobox__icons">
                          <?if(is_array($arItem['SERVICES'])):?>
                            <?foreach ($arItem['SERVICES'] as $icon):?>
                              <div class="infobox__icon" title="<?=$icon['NAME']?>">
                                <img src="<?=$icon['ICON']['SRC']?>"
                                     width="<?=$icon['ICON']['WIDTH']?>"
                                     height="<?=$icon['ICON']['HEIGHT']?>"
                                     alt=""/>
                              </div>
                            <?endforeach;?>
                          <?endif;?>
                        </div>
                      <?endif?>
                    </div>
                </div>
                <a href="" class="infobox-btn-showall infobox-btn-showall--region">Все на карте</a>
            </div>
        <? endforeach; ?></div>
<script>
	MapConstructor({id: 'i-map'}).setParams();
    MapConstructor({id: 'i-map'}).destroyPoints().addPointAsync(<?=CUtil::PhpToJSObject($arResult['POINTS'])?>);
</script>



<script>
	$(".infobox__title").on('click', function() {
		$(".infoboxPoint").css("display","none");
		MapConstructor({id: 'i-map'}).setCoordsTo();
		return false; 
	});

	$('body').on('click', '.infobox-btn-close', function(){
		document.location.reload(false);
	});
</script>
<? endif; ?>


