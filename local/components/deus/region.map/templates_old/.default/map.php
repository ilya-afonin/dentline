<?
  use Bitrix\Main\Loader;
  use Bitrix\Main\Localization\Loc;
  use Bitrix\Main\Page\Asset;

  if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
  }
  if (!Loader::includeModule("iblock"))
    return;

  Loc::loadMessages(__FILE__);
  Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/tpl/assets/css/lib/scrollbar.css");
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/assets/js/custom.map.js", true);
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/assets/js/infobox.js", true);
  Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/tpl/assets/js/lib/scrollbar.js", true);



  //Asset::getInstance()->addJs("https://maps.googleapis.com/maps/api/js?key=&callback=MapConstructor.load", true);

?>
<div class="map-wrapper">
  <div id="i-map" class="map i-map">
    <div id="userUI"></div>
  </div>
  <div class="map-info__opener">
    <div class="map-info__opener-text"><?= Loc::getMessage('I_MAP_TITLE'); ?></div>
  </div>

  <div class="map-info map-info--interactive">
    <div class="map-info__title"><?= Loc::getMessage('I_MAP_TITLE'); ?></div>
    <?
      $selectedCategory = !empty($_REQUEST['category'])? explode(':',$_REQUEST['category']):array();
      $selectedCategory = array_unique($selectedCategory);

      foreach($selectedCategory as $key=>$arCategory){
        $subArray[] = array("ID" => CIBlockElement::SubQuery("ID", array("IBLOCK_ID" => 7, "PROPERTY_SERVICES" => $arCategory, "PROPERTY_CITY"=>"%".$_REQUEST["ID_TO_SHOW"]."%")));
      }

      $rs = CIBlockElement::GetList(
          array(),
          array(
              "IBLOCK_ID" => 7,
              $subArray
          ),
          false,
          false,
          array("ID")
      );
      $elements_id = array();
      while ($ar = $rs->GetNext()) {
        $elements_id[] = $ar['ID'];
      }



		/*spoc добавил*/

		if(empty($_REQUEST["ID_TO_SHOW"]) && empty($_REQUEST["category"]) && isset($_REQUEST["category"]) && isset($_REQUEST["ID_TO_SHOW"]))
		{
			$APPLICATION->RestartBuffer();
			$APPLICATION->ShowHead();
		}

		if($_REQUEST["ID_TO_SHOW"] || $_REQUEST["category"])
		{
			$APPLICATION->RestartBuffer();
			if(empty($_REQUEST["ID_TO_SHOW"]))
				$APPLICATION->ShowHead();
		}
		/*---*/
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/jquery.jscrollpane.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/jquery.mousewheel.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/jScrollPane.js");
		



 	$GLOBALS['categoriesMapFilter'] = array("ID" => $elements_id);
      $APPLICATION->IncludeComponent("bitrix:news.list", "i-map", Array(
      "ACTIVE_DATE_FORMAT" => "d.m.Y",
      "CACHE_GROUPS" => "Y",
      "CACHE_TIME" => "300",
      "CACHE_TYPE" => "Y",
	//"FIELD_CODE" => array_merge((Array)$arParams['FIELD_CODE'], (Array)$arParams['PROPERTY_CODE']),
      "FIELD_CODE" => $arParams["FIELD_CODE"],
      "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
      "IBLOCK_ID" => $arParams['IBLOCK_ID'],
      "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
      "NEWS_COUNT" => "300",
      "SORT_BY1" => "ACTIVE_FROM",
      "SORT_BY2" => "SORT",
      "AJAX_MODE" => "Y",
      "AJAX_OPTION_HISTORY" => "N",
      "AJAX_OPTION_JUMP" => "N",
      "AJAX_OPTION_STYLE" => "Y",
      "SORT_ORDER1" => "DESC",
      "SORT_ORDER2" => "ASC",
      "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
      "FILTER_NAME"=>"categoriesMapFilter",
      "CATEGORY"=>$selectedCategory,

    ), $component);
	
	//var_dump($arResult["ITEMS"]);
    ?>
<style>
.map-info__categories {
    justify-content: space-between;
}
.map-info__category {
    margin-right: 0 !important;
}
.test {
    position: relative;
    margin-top: 0;
    min-height: 0;
    padding-bottom: 0;
    background-image: none;
    width: 95%;
    /*margin: 0 auto;*/
    outline: none;
    height: 27px;
}
.test2 {
    width: 100%;
    padding: 4px;
    border: none;
    /* float: right; */
    position: relative;
    overflow-y: scroll;
    /* text-align: right; */
    top:0;
    display: block;
    padding-left: 100px;
	color: #fff;
}
body input.spisok-gorodov-input{
height:15px;
}
input.spisok-gorodov-input {
    width: 90%;
    padding: 5px;
    text-align: left;
    border: none;
    /* float: left; */
    position: absolute;
    z-index: 5;
    background: transparent;
    background-color: transparent;
    background: #fff;
    margin-top: 1px;
    margin-left: 1px;
}
input#test2 {
    width: 288px;
    border: none;
    padding: 4px;
    text-align: left;
	color: #4b4b4b;
    font-family: "OpenSansLight";
    font-size: 16px;
    font-weight: 400;
	    box-sizing: border-box;
    padding-left: 10px;
}
.map-info__cities {
    clear: both;
}
.test2 option[selected] {
	color: #000;
}
option.opt_txt {
    color: grey;
}
select {
  width: auto;
  height: auto;
  border-radius: 10px;
  -webkit-appearance: none;
  background-image: url(/upload/images.png);
  background-position: 98% center;
  background-size: contain;
  background-repeat: no-repeat;
  line-height: 1em;
  /* for FF */
  -moz-appearance: none;
  text-indent: 0.01px; 
  text-overflow: '';
  /* for IE */
  -ms-appearance: none;
  appearance: none!important;
}
 
select::-ms-expand {
  display: none;
}
.jspContainer
 {
    position: relative;
    width: 100% !important;
}

.jspPane
{
    position: absolute;
    width: 100% !important;
    padding: 0 !important;
margin-left: 0 !important;
}

.jspVerticalBar
{
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
}
#mCSB_1 .mCSB_container{
overflow: visible;
}
.jspHorizontalBar
{
display:none;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 16px;
    background: red;
}

.jspCap
{
    display: none;
}

.jspHorizontalBar .jspCap
{
    float: left;
}
.test{
z-index:2000;
outline:1px solid grey;
}
div.test2{
	/*background:url(/upload/arrow-dw.png) no-repeat;*/
    background-position: 99% 10px;
    background-color: #fff;
    border-radius: 0;
	border: 1px solid #d6d6d6;
	outline: none;
    height: 27px;
    overflow: hidden;
    cursor: pointer;
    box-sizing: border-box;
	padding-left:0;
}
	div.test2 span:first-child{
    margin-top: 20px;
}
div.test2 span{
	color:#000;
	display: block;
	padding-left: 1px;
}
div.test2.oppener{
    height: auto;
    max-height: 360px;
    overflow-y: scroll;
	background: none;
    background-color: white;
}
.jspTrack
{
    background-color: #f4f4f5;
    position: relative;
    width: 4px !important;
    left: 98%;
}
.map-info__categories{
    position: relative;
    z-index: 40;
}
select.test2 option{
    padding: 1px 5px;
}
select.test2 option[selected] {
background-color:#fff;
}
.jspDrag{
    width: 4px;
    border-radius: 30px;
    background-color: #04a8e5;;
    position: relative;
    top: 0;
    left: 0px;
    cursor: pointer;
}

.jspHorizontalBar .jspTrack,
.jspHorizontalBar .jspDrag
{
    float: left;
    height: 100%;
}

.jspArrow
{
    background: #50506d;
    text-indent: -20000px;
    display: block;
    cursor: pointer;
    padding: 0;
    margin: 0;
}
.jspArrow.jspDisabled
{
    cursor: default;
    background: #80808d;
}

.jspVerticalBar .jspArrow
{
    height: 16px;
}
.jspContainer .jspPane{
		width:315px !important;
}
.jspHorizontalBar .jspArrow
{
    width: 16px;
    float: left;
    height: 100%;
}
.mCSB_container {
    margin-right: 30px;
}
.jspVerticalBar .jspArrow:focus
{
    outline: none;
}
#comments_block{
    overflow: visible !important;
}
.help_info {
    height: 520px;
width:auto !important;
}
.help_info_wrap{
margin-right:30px;
}
.jspCorner
{
    background: #eeeef4;
    float: left;
    height: 100%;
}
	.map-info__cities{
    position: relative;
    z-index: 10;
}
.infobox__icon > img
{
		/*display:none;*/
}
span.opt_txt {
    background: transparent;
}
span.opt_txt:hover {
    background: rgba(4, 168, 229, 0.5);
}
.clos-x {
    /*display: none;*/
	position: absolute;
    width: 20px;
    height: 24px;
    line-height: 24px;
    text-align: center;
    right: -25px;
	padding-left: 0px;
    padding-right: 0px;
	background-image: url(/upload/arrow-dw.png);
    background-repeat: no-repeat;
    background-position: right 80% center;
    color: transparent;
    /*-webkit-transform: rotate(180deg);
    transform: rotate(180deg);*/
	cursor: pointer;
}
.clos-x.oppen {
	display: block;
    background-image: url(/upload/arrow-dw.png);
    background-repeat: no-repeat;
    background-position: right 80% center;
    color: transparent;
	-webkit-transform: rotate(180deg);
    transform: rotate(180deg);
}
</style>
<?
/*spoc добавил*/
if((empty($_REQUEST["ID_TO_SHOW"])&& isset($_REQUEST["ID_TO_SHOW"])) || (empty($_REQUEST["category"]) && isset($_REQUEST["category"])))
	die();
if($_REQUEST["ID_TO_SHOW"])
	die();
/*-----*/
?>
  </div>
  <a href="#" class="map-move"></a>
</div>

<?
  function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
  }

  $zoom = 3;

  if(isMobile()) {
    $zoom = 2;
  }

  $arResult['JS_OBJECT'] = array(
      'id' => 'i-map',
      'options' => array(
          'zoom' => $zoom,
          'scrollwheel' => true,
          'gestureHandling' =>  'greedy',
          'disableDefaultUI' => true,
          'center' => array(
              47.993937,
              100.77914299999998
          ),
          'mapTypeControl' => true,
          'mapTypeId' => 'roadmap'
      ),
      'shops' => array()
  );
?>


<script>
  new MapConstructor(<?=CUtil::PhpToJSObject($arResult['JS_OBJECT'])?>).mapInitialize();


$(document).on("click", ".opt_txt", function (){
	$.ajax({
		url: '<?$APPLICATION->GetCurPage();?>',
		type: "POST",
		data: {ID_TO_SHOW:$(this).text(),category: 664},
	success: function(data){
		var p ='<div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside"><div class="map-info__title">Партнеры программы в регионах: </div>'+data+'</div>';
		$(".map-info.map-info--interactive").html(p);
    $(function()
    {
    	$('#mCSB_1').jScrollPane();
    });
	},
		error: function(msg){
	}
	});
});

$(document).on("keyup", "#searchInput", function (){
	$(".test2").addClass("oppener");
	
	var _this = $(this);
	$( ".opt_txt" ).each(function( ) {
		if(($(this).text().toLowerCase().indexOf(_this.val().toLowerCase())+1)>0) {
			$(this).css("display","block");
		}else{
			$(this).css("display","none");
		}
	});
	$( ".map-info__city" ).each(function( ) {
		if(($(this).find(".map-info__city-title").text().toLowerCase().indexOf(_this.val().toLowerCase())+1)>0) {
			$(this).css("display","block");
		}else{
			$(this).css("display","none");
		}
	});
});


	/*
$(document).on("change", "#searchInput", function (){
	$.ajax({
		url: '<?$APPLICATION->GetCurPage();?>',
		type: "POST",
		data: {ID_TO_SHOW:$(this).val(),category: 664},
	success: function(data){
		var p ='<div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside"><div class="map-info__title">Партнеры программы в регионах: </div>'+data+'</div>';
		$(".map-info.map-info--interactive").html(p);
    $(function()
    {
    	$('#mCSB_1').jScrollPane();
    });
	},
		error: function(msg){
	}
	});
});
*/

$(document).on("click", ".map-show-all", function (event){
var reset = false;
	if(!reset)
	{
		reset = true;
		/*$.ajax({
			url: '<?$APPLICATION->GetCurPage();?>',
			type: "POST",
			data: {ID_TO_SHOW:"",category: ""},
			success: function(data){
				var p ='<div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside"><div class="map-info__title">Партнеры программы в регионах: </div>'+data+'</div>';
				$(".map-info.map-info--interactive").html(p);
			$(function()
			{
				$('#mCSB_1').jScrollPane();
			});
			},
				error: function(msg){
			}
});*/
document.location.reload(false);

	}
});

/*$(".test2").on("click", function (){
	$(".test2.oppener").toggle(display);
});*/

$(document).on("click", ".clos-x", function (){
	$(this).addClass("oppen");
	$("div.test2").addClass("oppener");
	$("div.test2 span").css({"padding-top":"5px","padding-bottom":"5px"});
	$("div.test2 span:first-child").css({"display":"none"});
	$("div.test2.oppener").css({"top":"25px"});
	$("body input.spisok-gorodov-input").css({"width":"90%"});
	/*$(".clos-x").css({"display":"block"});*/
	/*$("div.test2.oppener").css({"bacground":"none"});
	$("div.test2").css({"bacground":"none"});*/

});

$(document).on("click", ".oppen", function (){
	$(".test2").removeClass("oppener");
	$(this).removeClass("oppen");
	$("div.test2").css({"top":"0px"});
	$("div.test2 span:first-child").css({"display":"block"});
	/*$(".clos-x").css({"display":"none"});*/
	$("div.test2 span:first-child").css({"display":"block"});
	/*$("div.test2").css({"background":"url(/upload/arrow-dw.png) no-repeat"});*/
});

$(".infobox__icon").each(function() {
  if(($(this).attr("title")) == "Список городов")
	{
		$(this).css({"display":"none"});
	}
});
/*
$("body").on("click", function (){
	$(".test2").removeClass("oppener");
});*/
$(document).ready(function() {
	$("#i-map > div > div > div:nth-child(1) > div:nth-child(3) > div").removeClass("gmnoprint");
});
$(document).on("click", ".gmnoprint", function (){
		$.ajax({
				url: "/ajaxCity.php",
				type: "POST",
				data: {	TITLE_OF_CITY:$(this).attr('title') },
				success: function(data)
				{
					var r = JSON.parse(data);
					//console.log();
						$(".map-info__cities .map-info__city").each(function() {
							if(r.indexOf($(this).find(".map-info__city-title").text()) == -1)
								$(this).css("display","none");
						});
				}
		});
		
	});

/*$(document).on("click", ".infobox-btn-showall.infobox-btn-showall--region", function (){
	$(".map-info__city").each(function() {
		$(this).css({"display":"block"}); 
	});
});*/
		
</script>



