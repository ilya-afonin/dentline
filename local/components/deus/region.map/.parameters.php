<?
  if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
  }
  use Bitrix\Main\Loader;
  use Bitrix\Main\Localization\Loc;
  use Bitrix\Iblock\IblockTable;
  use Bitrix\Iblock\TypeTable;
  use Bitrix\Iblock\PropertyTable;

  Loc::loadMessages(__FILE__);
  if(!Loader::includeModule('iblock')) {
    return;
  }
  $arIBlockTypeIterator = TypeTable::getList(array(
    'select' => array(
      'ID',
      'NAME' => 'LANG_MESSAGE.NAME'
    )
  ));
  $arBlockType = array();
  while($ar = $arIBlockTypeIterator->fetch()) {
    $arBlockType[$ar['ID']] = $ar['NAME'];
  }

  $arIBlock = array();
  if(isset($arCurrentValues["IBLOCK_TYPE"])){
    $rsIBlockIterator = IblockTable::getList(array(
      'order' => array("SORT" => "ASC"),
      'filter'=>array(
        "=IBLOCK_TYPE_ID" => $arCurrentValues["IBLOCK_TYPE"],
        "=ACTIVE" => "Y"
    )
    ));
    while($arr = $rsIBlockIterator->fetch()) {
      $arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
    }

  }




  $properties = array();
  if(intval($arCurrentValues['IBLOCK_ID']) > 0) {
    $propIterator = PropertyTable::getList(
      array(
      'select'=>array('ID', 'CODE', 'NAME','PROPERTY_TYPE'),
        'filter' => array('=IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], '=ACTIVE' => 'Y'),
    ));
    while($ar =$propIterator->fetch()){
      /*if ($ar["PROPERTY_TYPE"] == PropertyTable::TYPE_ELEMENT)
      {
        $properties['PROPERTY_'.$ar['CODE'].'.NAME'] = '['.$ar['CODE'].']'.$ar['NAME'];
        continue;
      }*/
      $properties[$ar['CODE']] = '['.$ar['CODE'].']'.$ar['NAME'];
    }
  }


  $arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
      "IBLOCK_TYPE" => Array(
        "PARENT" => "BASE",
        "NAME" => Loc::getMessage("T_IBLOCK_DESC_LIST_TYPE"),
        "TYPE" => "LIST",
        "VALUES" => $arBlockType,
        "DEFAULT" => "news",
        "REFRESH" => "Y",
      ),
      "IBLOCK_ID" => Array(
        "PARENT" => "BASE",
        "NAME" => Loc::getMessage("T_IBLOCK_DESC_LIST_ID"),
        "TYPE" => "LIST",
        "VALUES" => $arIBlock,
        "DEFAULT" => '',
        "MULTIPLE" => "N",
        "REFRESH" => "Y",
      ),
      "FIELD_CODE" => CIBlockParameters::GetFieldCode(Loc::getMessage("CP_BNL_FIELD_CODE"), "BASE"),
      "PROPERTY_CODE" => array(
        "PARENT" => "BASE",
        "NAME" => Loc::getMessage("CP_BNL_PROPS_CODE"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $properties,
        "ADDITIONAL_VALUES" => "Y",
      ),
    )
  );