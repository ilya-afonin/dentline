<?php
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

    use Bitrix\Main\Localization\Loc;

    $arComponentDescription = [
        "NAME" => Loc::getMessage("content_blocks_name"),
        "DESCRIPTION" => Loc::getMessage("content_blocks_description"),
        "PATH" => [
            "ID" => Loc::getMessage("content_blocks_id"),
            "CHILD" => [
                "ID" => "cake-blocks-content",
                "NAME" => Loc::getMessage("cake_blocks_content_name")
            ]
        ]
    ];
