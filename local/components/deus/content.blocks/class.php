<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Context;
use Bitrix\Main\Data\Cache;


class DeusContentBlocksComponent extends CBitrixComponent {

    public function onPrepareComponentParams($arParams)
    {
        $arParams["cacheId"] = "contentBlocks";
        $arParams["cacheTime"] = 3600;
        return $arParams;
    }

    private function getBlocks()
    {
        Loader::includeModule("iblock");

        $result = [];

        $cache = Cache::createInstance();
        if ($cache->initCache($this->arParams["cacheTime"], $this->arParams["cacheId"])) {
            $result = $cache->getVars();
        }
        elseif ($cache->startDataCache()) {

            $arSelect = Array("ID", "IBLOCK_ID", "NAME", 'ID', 'CODE', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'DETAIL_TEXT', 'PROPERTIES');
            $arFilter = Array("IBLOCK_ID" => 4, "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);

            while ($arRes = $res->GetNextElement()){

                $el = $arRes->GetFields();
                $el["PROPERTIES"] = $arRes->GetProperties();
                $result[$el['CODE']] = $el;
            }

            $cache->endDataCache($result);
        }

        return $result;

    }


    public function executeComponent()
    {

        $blocks = $this->getBlocks();
        if (isset($blocks[$this->arParams['identity']])) {
            $this->arResult = $blocks[$this->arParams['identity']];
            $this->includeComponentTemplate();
        }
    }
}