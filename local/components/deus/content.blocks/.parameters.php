<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

    Loader::includeModule("iblock");

    $arElems = array();

    $blocks = \Bitrix\Iblock\ElementTable::getList([
        'select' => ['ID', 'CODE', 'NAME', 'PREVIEW_TEXT'],
        'filter' => ['ACTIVE' => 'Y', 'IBLOCK_ID' => 4]
    ])->fetchAll();

    foreach ($blocks as $block) {
      $arElems[$block['CODE']] = "[" . $block["CODE"] . "] " . $block["NAME"];
    }

    $arComponentParameters = array(
        "PARAMETERS" => array(
            "identity" => array(
                "NAME" => Loc::getMessage("identity"),
                "TYPE" => "LIST",
                "VALUES" => $arElems,
                "DEFAULT" => '1',
                "MULTIPLE"=>"N",
                "ADDITIONAL_VALUES"=>"N",
                "PARENT" => "BASE",
                "REFRESH" => "Y",
            ),
            "invert" => array(
                "NAME" => Loc::getMessage("invert"),
                "TYPE" => "CHECKBOX",
                "DEFAULT" => "N",
                "PARENT" => "BASE",
            )
        )
    );
