<?php

$img = CFile::ResizeImageGet(
    $arResult['PREVIEW_PICTURE'],
    array("width" => 350, "height" => 345),
    BX_RESIZE_IMAGE_EXACT,
    true,
    false
);
$arResult['PREVIEW_PICTURE'] = array("big" => CFile::GetPath($arResult['PREVIEW_PICTURE']), "small" => $img['src']);

$pictures = array();

if (!empty($arResult["PROPERTIES"]["MORE_PHOTOS"]["VALUE"])){
    foreach ($arResult["PROPERTIES"]["MORE_PHOTOS"]["VALUE"] as $photo){
        $s = CFile::ResizeImageGet(
            $photo,
            array("width" => 160, "height" => 160),
            BX_RESIZE_IMAGE_EXACT,
            true,
            false
        );
        if ($s){
            $pics[] = array("big" => CFile::GetPath($photo), "small" => $s['src']);
        }
    }
}

$arResult['PROPERTIES']["MORE_PHOTOS"]['VALUE'] = $pics;