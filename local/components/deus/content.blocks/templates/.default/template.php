<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
?>
<div class="block__content<?=($arParams['invert'])?' block__content--left':''?>">
    <div class="block__image wow fadeIn">
        <?if(is_array($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'])):?>
            <div class="block__images-wrapper">
                <?foreach ($arResult['PROPERTIES']['MORE_PHOTOS']['VALUE'] as $k => $photo):?>

                    <?if(!empty($arResult['PROPERTIES']['MORE_PHOTOS']['DESCRIPTION'][$k])): ?>
                        <img class="block__images-video" src="<?=$photo['small']?>" alt="<?=$arResult['NAME']?>" data-fancybox
                             data-src="<?=$arResult['PROPERTIES']['MORE_PHOTOS']['DESCRIPTION'][$k]?>">
                    <?else:?>
                        <img src="<?=$photo['small']?>" alt="<?=$arResult['NAME']?>" data-fancybox data-src="<?=$photo['big'];?>">
                    <?endif;?>

                <?endforeach;?>
            </div>
        <?elseif(!empty($arResult['PREVIEW_PICTURE'])):?>
            <img src="<?=$arResult['PREVIEW_PICTURE']['small']?>" alt="<?=$arResult['NAME']?>" data-fancybox
                 data-src="<?=$arResult['PREVIEW_PICTURE']['big']?>">
        <?endif;?>
    </div>
    <div class="block__text">
        <div class="block__title">
            <?=$arResult['PREVIEW_TEXT']?>
        </div>
        <p class="block__desc">
            <?=$arResult['DETAIL_TEXT']?>
        </p>
    </div>
</div>
